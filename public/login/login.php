<div class="container" style="margin-top: 10%">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card-group">
        <div class="card p-4">
          <div class="card-body">
            <formintelirec.png>
              <h1> <img class="navbar-brand-full" src="<?php echo URI_IMAGES; ?>intelirec.png" width="200" height="70" alt="CoreUI Logo"></h1>
              <p class="text-muted">Sua receita médica inteligente!</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-user"></i>
                  </span>
                </div>
                <input class="form-control" type="text" id="email" placeholder="Email">
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                <input class="form-control" type="password" id="password" placeholder="Senha">
              </div>
              <div class="row">
                <div class="col-6">
                  <button class="btn btn-primary px-4" id="login-button" type="button">Login</button>
                </div>
                <div class="col-6 text-right">
                  <button class="btn btn-link px-0" type="button">Esqueceu sua senha?</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
          <div class="card-body text-center">
            <div>
              <h2>Registre-se</h2>
              <p>É um médico ou dono de farmácia e ainda não usa o Intelirec? Ligue para nossa central de atendimento e saiba como se cadastrar (0800-000000)</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>