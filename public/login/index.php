<?php
require_once(dirname(__FILE__) . '/../../application/config/global.php');
require_once(PATH_HELPERS . 'TemplateHandler.php');
if(isset($_COOKIE['authorized'])){
    header("location:" . URI_DASHBOARD);
}
$template = new TemplateHandler();
$template->set_css_files(array());
$template->set_js_files(array('login.js'));
$template->set_content_file('login/login.php');
$template->show();
