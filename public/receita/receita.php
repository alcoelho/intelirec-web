<div class="card">
  
  <div class="card-body">
    <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;"></div>
    <form onsubmit="getMedicamento(event)">
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_receita">Código único de receita</label>
          <input class="form-control" id="id_receita" type="text" placeholder="Código único de receita">
        </div>
      </div>
      <!--       <div class="row">
        <div class="form-group col-sm-6">
          <label for="medicamento">Medicamento</label>
          <input class="form-control" id="medicamento" type="text" placeholder="Nome do medicamento">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="classe">Classe</label>
          <input class="form-control" id="classe" type="text" placeholder="Classe">
        </div>
      </div> -->
      <div class="row">
        <div class="form-group col-sm-2">
          <label for="paciente_cpf">CPF</label>
          <input class="form-control" id="paciente_cpf" type="text" placeholder="CPF do Paciente">
        </div>
        <!--     <div class="form-group col-sm-2">
          <label for="data_inicio">Início</label>
          <input class="form-control" id="data_inicio" type="date" placeholder="Data de início">
        </div>
        <div class="form-group col-sm-2">
          <label for="data_fim">Fim</label>
          <input class="form-control" id="data_fim" type="date" placeholder="Data de fim">
        </div> -->
      </div>
      <div class="form-actions" style="margin:0 0 2% 0">
        <button class="btn btn-primary" type="submit">Filtrar</button>
        <a class="btn btn-secondary" type="button" href="javascript:clear()">Limpar</a>
      </div>
    </form>
    <div class="row" id="hist_table">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Histórico de receitas</div>
          <div class="card-body">
            <br>
            <table class="table table-responsive-sm table-hover table-outline mb-0" id="tab_receitas">
              <thead class="thead-light">
                <tr>
                  <th>Id</th>
                  <th>
                    <i class="icon-people"></i> Paciente
                  </th>
                  <th>CPF do paciente</th>
                  <th>Data</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    <div class="modal fade" id="receitaDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width: 60%; max-width: 60%">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Detalhes da receita</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body" id="receita_detalhes_texto">
          </div>
          <div class="modal-footer">
            <a class="btn btn-danger" onclick="updateReceita('Invalidated');" permissions="FARMA" id="btn_rejeitar" href="#">Rejeitar receita</a>
            <a class="btn btn-primary" permissions="FARMA" id="btn_utilizar" onclick="updateReceita('Processed');">Informar utilização da receita</a>
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Fechar</button>
          </div>
        </div>
        <!-- /.modal-content-->
      </div>
      <!-- /.modal-dialog-->
    </div>
    
  </div>
</div>