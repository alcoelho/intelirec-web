<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;"></div>
        <div class="alert alert-success" role="alert" id="msg_sucess_top" style="display: none;"></div>
        <form onsubmit="saveReceita(event)">
          <div class="row">
            <div class="form-group col-sm-2">
              <label for="data">Prescrita em:</label>
              <input class="form-control" id="data" type="text" disabled="disabled">
            </div>
          </div>
          <fieldset>
            <legend>Dados do médico/clínica</legend>
            <div class="row">
              <div class="form-group col-sm-8">
                <label for="medico_nome">Nome</label>
                <input class="form-control" id="medico_nome" type="text" disabled="disabled">
              </div>
              <div class="form-group col-sm-4">
                <label for="medico_crm">CRM</label>
                <input class="form-control" id="medico_crm" type="text" disabled="disabled">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="medico_endereco">Endereço</label>
                <input class="form-control" id="medico_endereco" type="text" disabled="disabled">
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend>Dados do paciente</legend>
            <div class="form-group">
              <label for="paciente_nome">Nome completo</label>
              <input class="form-control" id="paciente_nome" type="text" placeholder="Nome">
            </div>
            <div class="row">
              <div class="form-group col-sm-8">
                <label for="paciente_cpf">CPF</label>
                <input class="form-control" id="paciente_cpf" type="text" placeholder="CPF">
              </div>
              <div class="form-group col-sm-4">
                <label for="paciente_rg">RG</label>
                <input class="form-control" id="paciente_rg" type="text" placeholder="RG">
              </div>
            </div>
            <div class="form-group">
              <label for="paciente_endereco">Endereço</label>
              <input class="form-control" id="paciente_endereco" type="text" placeholder="Endereço">
            </div>
          </fieldset>
          <fieldset>
            <legend>Medicamentos</legend>
            <div class="row">
              <div class="form-group col-sm-12">
                <h5>Inserir medicamento</h5>
                <div class="alert alert-success" role="alert" id="msg_sucess" style="display: none;"></div>
              </div>
              <div class="form-group col-sm-12">
                <label for="medicamento_medicamento">Medicamento</label>
                <input class="form-control" id="medicamento_medicamento" type="text" placeholder="Medicamento">
              </div>
              <!--                           <div class="form-group col-sm-4">
                <label for="medicamento_dosagem">Dosagem</label>
                <input class="form-control" id="medicamento_dosagem" type="text" placeholder="Medicamento">
              </div> -->
              <div class="form-group col-sm-12">
                <label for="medicamento_instrucoes">Instruções de uso</label>
                <input class="form-control" id="medicamento_instrucoes" type="text" placeholder="Instruções de uso">
                <input type="hidden" id="medicamento_cod" value="">
              </div>
              <div class="form-group col-sm-4">
                <a class="btn btn-outline-success" id="btnAddMed" href="javascript:addMedicamento()">Adicionar</a>
                <a class="btn btn-outline-secondary" id="btnCancel" href="javascript:auxClearEditRemedio()" style="display: none;">Cancelar</a>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <h5>Medicamentos inseridos</h5>
                <table class="table table-responsive-sm" id="tab_medicamentos">
                  <thead>
                    <tr>
                      <th>Medicamento</th>
                      <th>Instrucões de uso</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </fieldset>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Salvar</button>
            <button class="btn btn-secondary" type="button">Cancelar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>