<?php
require_once(dirname(__FILE__) . '/../../application/config/global.php');
require_once(PATH_HELPERS . 'TemplateHandler.php');
if(!isset($_COOKIE['authorized'])){
    header("location:" . URI_ROOT);
}

$template = new TemplateHandler();

$template->set_page_title('<span>Receita - Histórico</span>');

$template->set_css_files(array('receita.css'));
$template->set_js_head_files(array());
$template->set_js_files(array(
    'receita.js'
));
$template->set_content_file('receita/receita.php');
$template->show();