<?php
ignore_user_abort(true);

require_once(dirname(__FILE__) . '/../../application/config/global.php');
require_once(PATH_HELPERS . 'LogException.php');

$description = isset($_POST['description']) ? $_POST['description'] : '';

if (isset($_FILES['attachment'])) {
    $extension = pathinfo($_FILES['attachment']['tmp_name'], PATHINFO_EXTENSION);
    $target = PATH_TEMPORARY . 'attachment_' . time();
    $target.= $extension ? '.' . $extension : '';
    if (move_uploaded_file($_FILES['attachment']['tmp_name'], $target)) {
        throw new LogException($description, 0, null, $target);
    }
}

throw new LogException($description);