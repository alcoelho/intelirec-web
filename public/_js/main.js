var api = APIRequest.getInstance();
api.baseURL = URI_API; // This constant is defined in templates/global.php

$(function(e) {
    mainLoad();
    checkPermissions();
});

function mainLoad() {
    $('#email-login').html(localStorage.email);
}

function getDateHourFromUTF8(utf8format) {

    var dh = '';

    var dateSplit = utf8format.split('T');

    var dh = dateSplit[0] + ' ' + dateSplit[1].substring(0, 5);
    return dh;
}

function getRegraType(regra_type)
{
    var regra = "";

    switch(regra_type)
    {
        case "CRM_SUSP":
        regra = "CRM Suspenso";
        break;
        case "REMED_PROIBIDO":
        regra = "Remédio proibido";
        break;
    }

    return regra;
}

function getStatus(status)
{
    var statusRet = "";

    switch(status)
    {
        case "Prescribed":
        statusRet = "Prescrita";
        break;
        case "Proccessed":
        statusRet = "Utilizada";
        break;
        case "Invalidated":
        statusRet = "Invalidada";
        break;

    }

    return statusRet;
}

function showMsg(msg, type) {
    if (type == 'ERROR') {
        $('#msg_header').css("background", "#df5b5b");
        $('#msg_header').css("color", "#FFF");
        $('#msg_title').html('Existem erros na sua ação');
    } else {
        $('#msg_header').css("background", "#FFF");
        $('#msg_header').css("color", "#000");
        $('#msg_title').html('Operação realizada com sucesso');
    }
    $('#msg_msg').html(msg);
    $('#msg_dialog').modal('show');
}

function medicamentoAutoComplete(input, hidden) {
    var options = {
        url: function(p) {
            var split = p.split(' - ');
            var product = split[0];
            var apresentacao = split.length > 1 ? split[1] : "";
            return "http://mobile-aceite.tcu.gov.br/mapa-da-saude/rest/remedios?produto=" + product + "&apresentacao=" + apresentacao + "&quantidade=30";
        },

        getValue: function(element) {
            return element.produto + " - " + element.apresentacao;
        },

        list: {
            maxNumberOfElements: 30,
            match: {
                enabled: true
            },
            onSelectItemEvent: function() {
                var selectedItemValue = $(input).getSelectedItemData().cod;
                $(hidden).val(selectedItemValue);
            }
        }

    };

    $(input).easyAutocomplete(options);

}

function checkPermissions() {

    $("[permissions]").each(function(index) {
        auxCheckPermissions($(this).attr('permissions'), $(this))
    });

    if (typeof PAGE_PERMISSION !== 'undefined') {
        auxCheckPermissions(PAGE_PERMISSION, null)
    }

}

function auxCheckPermissions(permissions, element) {

    var profile = localStorage.profile;

    if (permissions.indexOf(',') == -1) {
        if (permissions != profile) {

            if (element != null)
                element.remove();
            else
                window.history.back();
        }
    } else {
        var splitPermissions = permissions.split(',');

        if (!splitPermissions.includes(profile)) {
            if (element != null)
                element.remove();
            else
                window.history.back();
        }

    }
}


function logout() {
    document.cookie = 'authorized=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/';
    localStorage.clear();
    location.reload();
}