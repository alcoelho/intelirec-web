
loadForm();
Blockchain.init();

function loadForm() { }

$("#regra_type").change(function () {
    switch ($('#regra_type').val()) {
        case 'REMED_PROIBIDO':
            $('#dados_medico').hide();
            $('#dados_medicamento').show();
            medicamentoAutoComplete("#medicamento_medicamento", "#medicamento_cod");
            break;
        case 'CRM_SUSP':
            $('#dados_medico').show();
            $('#dados_medicamento').hide();
    }
});

function auxClearAllFields() {
    $('#regra_type').val("");
    $('#regra_type').prop('selectedIndex', 0);
    $('#regra_type option:first').prop('selected', true);
    $("#medico_nome").val("");
    $("#medico_crm").val("");
    $("#medicamento_medicamento").val("");
    $("#medicamento_cod").val("");
}

function getId() {
    var id = "";

    id = Math.floor((Math.random() * 10) + 1).toString() + Math.floor((Math.random() * 10) + 1).toString() + Math.floor((Math.random() * 10) + 1).toString() + Math.floor((Math.random() * 10) + 1).toString();
    return parseInt(id);

}

function saveRegra(event) {
    event.preventDefault();

    if ($('#regra_type').val() == "") {
        showMsg("Campo obrigatório não informado", 'ERROR');
    }

    var regra = {};

    regra.id = getId();
    regra.regra_type = $('#regra_type').val();
    regra.medico = $("#medico_nome").val();
    regra.crm = $("#medico_crm").val();
    regra.medicamento = $("#medicamento_medicamento").val();
    regra.medicamento_id = $("#medicamento_cod").val();

    if (regra.regra_type == 'REMED_PROIBIDO') {
        Blockchain
            .addProhibitedMedicine(regra.medicamento_id)
            .then(function (hash) {
                regra.blockchain_hash = hash;
                api.call({
                    url: "/regras",
                    type: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(regra),
                    dataType: 'json',
                    handleError: true,
                    success: function(data, textStatus, jqXHR) {
                        auxClearAllFields();
                        loadForm();
                        showMsg("Regra registrada com sucesso", 'SUCCESS');
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        showMsg("Erro ao registrar regra", 'ERROR');
                    }
                }, true);
            })
            .catch(function (err) {
                console.error(err);
                showMsg("Erro ao registrar regra na Blockchain", 'ERROR');
            });
    }
   
}