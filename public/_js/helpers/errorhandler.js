function ErrorHandler() {}

/*
 * Constants
 */

ErrorHandler.STATUS_HTML2CANVAS_LOADED = 1;
ErrorHandler.STATUS_SCREENSHOT_RENDERED = 2;
ErrorHandler.STATUS_LOG_SAVED = 3;


/*
 * Global handler (catch unhandled Javascript errors)
 */

/*window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
    // Build the description
    var description = '<h4>Line ' + lineNumber + '</h4>' + url;
    description+= '<h4>Message</h4>' + errorMsg;
    // Call the generic handler
    ErrorHandler.handleError(description);
    // Suppress error alert
    return true;
};*/


/*
 * Handle errors returned on form submission
 */

ErrorHandler.handleFormError = function(requestSettings, jqXHR, form, customHandler) {
    var target = form.find('button[type=submit]');
    var retry = function() {
        form.submit();
    };
    // Forbidden (invalid fields)
    if (jqXHR.status == 403 && jqXHR.responseText) {
        try {
            var data = $.parseJSON(jqXHR.responseText);
            if ($.isPlainObject(data.error)) {
                // Print error message
                if (target) {
                    if (Object.keys(data.error).length > 1) {
                        addErrorTooltip(target, {
                            content: 'Please check the input fields'
                        });
                    } else {
                        addErrorTooltip(target, {
                            content: 'Please check the input field'
                        });
                    }
                }
                if (customHandler && typeof(customHandler) == 'function') {
                    customHandler(data.error);
                } else {
                    highlightInvalidFields(data.error);
                }
            } else {
                if (typeof data.error == 'string') {
                    if (target) {
                        addErrorTooltip(target, {
                            content: data.error
                        });
                    }
                } else {
                    throw 'invalidresponse';
                }
            }
        } catch (e) {
            ErrorHandler.handleRequestError(requestSettings, jqXHR, retry);
            return false;
        }
    } else {
        ErrorHandler.handleRequestError(requestSettings, jqXHR, retry);
        return false;
    }
};


/*
 * Handle AJAX response errors 
 */

ErrorHandler.handleRequestError = function(requestSettings, jqXHR, retry, openModal) {

    var errorList = '';

    if (jqXHR.responseJSON != undefined) {
        if (typeof jqXHR.responseJSON.error == 'object') {

            $.each(jqXHR.responseJSON.error, function(key, value) {
                errorList += '<span class="key-error">' + key + '</span> : <span>' + value + '</span><br>';
                console.log(key + value);
                //now below here, I want to perform action, based on the values i got as key and values
            });
        } else {
            errorList +=  jqXHR.responseJSON.error != undefined ? '<span class="key-error">'+ jqXHR.responseJSON.error  + '</span><br>' : '';
        }
    }

    // Build the description
    var description = '<h4>' + requestSettings.type + '</h4>' + requestSettings.url;
    if (requestSettings.data) {
        description += '<h4>Data</h4>' + requestSettings.data;
    }
    description += '<h4>Response (HTTP ' + jqXHR.status + ' ' + jqXHR.statusText + ')</h4>' + jqXHR.responseText;
    // Call the generic handler
    ErrorHandler.handleError(description, retry, openModal, errorList);
};


/*
 * Handle generic errors
 */



ErrorHandler.handleError = function(description, retry, openModal, errorList) {
    // Open error modal
    var showModal = function() {
        var modalOpened = false;
        openModal = (typeof(openModal) == 'undefined') ? true : false;
        if (!modalOpened && openModal) {
            ErrorHandler.openErrorModal(retry, errorList);
            modalOpened = true;
        }
    };
    // Log
    ErrorHandler.log(description)
        .progress(function(level) {
            if (level == ErrorHandler.STATUS_SCREENSHOT_RENDERED) {
                showModal();
            }
        })
        .always(function() {
            showModal();
        });
};



/*
 * Try to take a screenshot and report the error
 */

ErrorHandler.log = function(description) {
    return new $.Deferred(function(deferred) {
        // Try to prevent from navigating away or closing the window before log is saved
        window.onbeforeunload = function(e) {
            var message = 'Reporting an issue. Please wait.',
                event = (e || window.event);
            // For IE and Firefox
            if (event) {
                event.returnValue = message;
            }
            // For Safari
            return message;
        };
        // Make a AJAX request to save the log
        var saveLog = function(screenshot) {
            var data = new FormData();
            data.append('description', description);
            if (screenshot) {
                data.append('attachment', screenshot);
            }
            $.ajax({
                url: URI_LOG,
                type: 'POST',
                processData: false,
                contentType: false,
                data: data
            }).done(function() {
                deferred.notify(ErrorHandler.STATUS_LOG_SAVED);
                deferred.resolve();
                window.onbeforeunload = null;
            }).fail(function() {
                deferred.reject();
            });
        };
        // Take a screenshot of the current page
        var takeScreenshot = function() {
            html2canvas(document.body, {
                onrendered: function(canvas) {
                    // Crop if window is wider than #main + padding
                    var width = $('#main').width() + 40;
                    if (canvas.width > width) {
                        var extraCanvas = document.createElement('canvas');
                        var offsetX = canvas.width / 2 - width / 2;
                        extraCanvas.setAttribute('width', width);
                        extraCanvas.setAttribute('height', canvas.height);
                        var context = extraCanvas.getContext('2d');
                        context.drawImage(canvas, offsetX, 0, width, canvas.height, 0, 0, width, canvas.height);
                        canvas = extraCanvas;
                    }
                    // Convert the canvas to a Blob object, to be send on the request
                    canvas.toBlob(function(blob) {
                        deferred.notify(ErrorHandler.STATUS_SCREENSHOT_RENDERED);
                        saveLog(blob);
                    }, 'image/jpeg', 0.5);
                }
            });
        };
        // Load CanvasToBlob lib
        var loadCanvas2Blob;
        var CanvasPrototype = window.HTMLCanvasElement && window.HTMLCanvasElement.prototype;
        if (!CanvasPrototype.toBlob) {
            loadCanvas2Blob = $.ajax({
                dataType: 'script',
                cache: true,
                url: URI_JS + 'libraries/canvas2blob.js'
            });
        }
        // Load HTML2Canvas lib
        var loadHTML2Canvas;
        if (!window.html2canvas) {
            loadHTML2Canvas = $.ajax({
                dataType: 'script',
                cache: true,
                url: URI_JS + 'libraries/html2canvas.js'
            });
        }
        // Wait until both libs are loaded
        $.whenAlways(loadCanvas2Blob, loadHTML2Canvas)
            .done(function() {
                deferred.notify(ErrorHandler.STATUS_HTML2CANVAS_LOADED);
                // Take a screenshot and then save the log
                takeScreenshot();
            })
            .fail(function() {
                // Save the log
                saveLog();
            });
    }).promise();
};


/*
 * Show an error message with the option to try again
 */
ErrorHandler.openErrorModal = function(retry, errorList) {
    // Present the modal

    var title = 'Unexpected response from server';
    var msg = 'The system administrator has been notified.';
    var btnRetry = '<button type="button" class="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--accept mdc-dialog__action" id="retry">Retry</button>';

    if (errorList !== '') {
        msg = errorList;
        title =  translate('there_are_errors_in_your_action','There were errors in your action.');
        btnRetry = '';
    }

    if (!$('#dialog_log_erro').length) {
        $('body').append('\
            <aside id="dialog_log_erro" class="mdc-dialog">\
                <div class="mdc-dialog__surface">\
                    <header class="mdc-dialog__header header-error">\
                        <h2 id="my-mdc-dialog-label" class="mdc-dialog__header__title">' + title + '</h2>\
                    </header>\
                    <section id="my-mdc-dialog-description" class="mdc-dialog__body">\
                        <p>' + msg + '</p>\
                    </section>\
                    <footer class="mdc-dialog__footer">\
                        <button type="button" id="close_dialog_log_erro" class="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--cancel">Ok</button>' + btnRetry +
            '</footer>\
                </div>\
                <div class="mdc-dialog__backdrop"></div>\
                </aside>');
    }
    else
    {
       $('#my-mdc-dialog-description p').html(msg);
    }

    $('#retry').hide();


    var dialog = new mdc.dialog.MDCDialog(document.querySelector('#dialog_log_erro'));

    dialog.listen('MDCDialog:accept', function() {
        console.log('accepted');
    })

    dialog.listen('MDCDialog:cancel', function() {
        console.log('canceled');
    })

    dialog.show();

    // Handle retry
    if (retry) {
        $('#retry').show().click(function() {
            dialog.hide();
            $('#dialog_log_erro').remove();
            retry();
        });
    }
};

$('#close_dialog_log_erro').click(function() {
    var dialog = new mdc.dialog.MDCDialog(document.querySelector('#dialog_log_erro'));
    dialog.hide();
    $('#dialog_log_erro').remove();
});

/*
 *  Helper functions
 */

ErrorHandler.highlightInvalidFields = function(fields) {
    $('.alert').remove();
    $.each(fields, function(field, description) {
        field = $('#' + field);
        if (field[0]) {
            if (description) {
                var alert =
                    '<div class="alert alert-danger">' +
                    '    <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    description +
                    '</div>';
                field.parent().append(alert);
            }
            if (field.parent().hasClass('form-group')) {
                field.parent().addClass('has-error');
            }
        }
    });


};