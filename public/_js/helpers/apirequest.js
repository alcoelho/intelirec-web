function default_date(date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}


function encode(text, key) {
    switch (key) {
        case "BRACKET":
            return text.replace("[", "%5B").replace("]", "%5D");
            break;
        default:
            break;
    }
}

function formateDateToFilter(date) {

    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    return yyyy + '-' + mm + '-' + dd;
}

function createUtcDate(datetime) {

    var dateTime = datetime.split(" "); //dateTime[0] = date, dateTime[1] = time

    var date = dateTime[0].split("-");
    var time = dateTime[1].split(":");

    new_date = new Date();
    new_date.setUTCFullYear(date[0]);
    new_date.setUTCMonth((date[1] - 1));
    new_date.setUTCDate(date[2]);

    new_date.setUTCHours(time[0]);
    new_date.setUTCMinutes(time[1]);
    new_date.setUTCSeconds(time[2]);

    return new_date;
}

var APIRequest = (function() {

    function APIRequest() {

        // Properties
        this.baseURL = null;

        // Private variables
        var self = this;
        var queue = [];
        var currentRequest;
        var ajaxOptions = {
            timeout: 10000,
            dataType: 'json',
            traditional: true,
            handleError: true
        };

        /*
         * Call the API
         */
        this.call = function(customOptions, authorize) {
            if (!self.baseURL) {
                throw 'API base URL is not defined';
            }
            if (customOptions.complete) {
                throw 'Options cannot implement "complete" callback';
            }
            // Merge AJAX options
            var options = $.extend(true, {}, ajaxOptions, customOptions);
            // Prepend the API base URL
            options.url = self.baseURL + options.url;
            // Add custom parameters
            options.customOptions = customOptions;
            options.authorize = authorize;
            // Init the request
            performRequest(options, authorize);
        };

        /*
         * Perform an asynchronous HTTP (AJAX) request
         */
        var performRequest = function(options, authorize) {
            currentRequest = $.ajax(options);
            return true;
        };
    }

    // Singleton pattern
    var instance = null;
    return {
        getInstance: function() {
            if (instance === null) {
                instance = new APIRequest();
                // Hide the constructor so the returned object can't be new'd
                instance.constructor = null;
            }
            return instance;
        }
    };
})();