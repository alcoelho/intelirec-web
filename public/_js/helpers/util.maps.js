function boundParse2json(bound) {
        try {
            var _bound = new Object();
            _bound.SouthWest = new Object();
            _bound.SouthWest.lat = bound.getSouthWest().lat();
            _bound.SouthWest.lng = (bound.getSouthWest().lng() > bound.getNorthEast().lng()) ? -180 : bound.getSouthWest().lng();
            _bound.NorthEast = new Object();
            _bound.NorthEast.lat = bound.getNorthEast().lat();
            _bound.NorthEast.lng = (bound.getNorthEast().lng() < bound.getSouthWest().lng()) ? 180 : bound.getNorthEast().lng();
            return JSON.stringify(_bound);
        } catch (err) {
            return null;
        }
    }