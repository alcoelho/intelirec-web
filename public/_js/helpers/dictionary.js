var statusColors = [];
statusColors.push({
	'CREATED': '',
	'OPEN': '',
	'AVAILABLE': '',
	'RECEIVED': '',
	'SENT': '',
	'VERIFYLOST': '',
	'LOST': '',
	'DAMAGED': '',
	'DAMAGED_TO_DESTROY': '',
	'DAMAGED_TO_REPAIR': '',
	'APPLIED': '',
	'REPLACED': '',
	'APPLIED_GENERIC': '',
	'REMOVED_GENERIC': '',
	'RECEIVED_APPLIED_GENERIC': ''
});

function getStatusColor(text) {
	return statusColors[0][text];
}


var status_map = [];
status_map.push({
	'CREATED': ['CREATED'],
	'OPEN': ['OPEN'],
	'AVAILABLE': ['AVAILABLE'],
	'RECEIVED': ['RECEIVED'],
	'SENT': ['SENT'],
	'TO_SEND':['TO_SEND'],
	'VERIFYLOST': ['VERYFILOST'],
	'LOST': ['LOST'],
	'DAMAGED': ['DAMAGED'],
	'DAMAGED_TO_DESTROY': ['DAMAGED_TO_DESTROY'],
	'DAMAGED_TO_REPAIR': ['DAMAGED_TO_REPAIR'],
	'APPLIED': ['APPLIED', 'APPLIED_GENERIC'],
	'REPLACED': ['REPLACED', 'REPLACED_GENERIC'],
	'REMOVED_GENERIC': ['REMOVED', 'REMOVED_GENERIC'],
	'RECEIVED': ['RECEIVED', 'RECEIVED_APPLIED_GENERIC'],
	'DESTROYED': ['DESTROYED']
});

function getStatusMap(text) {

	var stausReturn = [];

	stausReturn = status_map[0][text] != undefined ? status_map[0][text] : [];

	return stausReturn;
}

var typesIcon = [];
typesIcon.push({
	"GENERIC_TAG": "",
	"GENERIC_SEAL": "",
	"SEAL": "",
	"BAG": "",
	"BOX": "",
	"METER": "",
	"METER_BAG": "",
	"METER_BOX": "",
	"GENERIC_TAG": "",
	"PACK": "",
	"PALLET": "",
	"GENERIC_SEAL": "",
	"LEGACY_METER": "",
	"LEGACY_METER_BOX": ""
});

function getTypeIcon(text) {
	return typesIcon[0][text];
}

var usersIocn = [];
usersIocn.push({
	"OWNER": "",
	"USER": "",
	"FACTORY": "",
	"ADMIN": "",
	"WAREHOUSE": "",
	"TRANSPORT": ""
});

function getUserIcon(text) {
	return usersIocn[0][text];
}