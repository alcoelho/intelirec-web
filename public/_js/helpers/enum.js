const user_types = [
    {
        id: 'USER',
        description: 'User'
    }, {
        id: 'FACTORY',
        description: 'Factory'
    }, {
        id: 'ADMIN',
        description: 'Admin'
    }, {
        id: 'WAREHOUSE',
        description: 'Warehouse'
    }, {
        id: 'TRANSPORT',
        description: 'Transport'
    }, {
        id: 'WASHER',
        description: 'Washer'
    },
    {   
        id: 'LAB',
        description: 'Lab'
    },
    {   
        id: 'CONTRACTOR',
        description: 'Contractor'
    },
    {   
        id: 'SUPERADMIN',
        description: 'Super admin'
    }];