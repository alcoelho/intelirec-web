/*
 * Default DataTables options
 */


/*oLanguage: {
            "sUrl": I18N == '' ? (URI_JS + 'helpers/lang/' + navigator.language.substr(0, 2) + '.json') : (URI_JS + 'helpers/lang/' + I18N + '.json')
},*/


var baseOptions = {
    oLanguage: {
        sSearch: '',
        sEmptyTable: 'No results',
        oPaginate: {
            sPrevious: ' ',
            sNext: ' '
        }
    },
    bInfo: false,
    bLengthChange: false,
    bServerSide: true,
    bDeferRender: true,
    iDisplayLength: 30,
    sPaginationType: 'full_numbers',
    fnInitComplete: function(oSettings) {
        // Add filtering delay
        var searchWaitInterval;
        // Disable loading state
        loadingModal.hide();
        $(this).show().parent().fadeIn();
        $('.dataTables_filter input').unbind();
        $('.dataTables_filter input').keyup(function(e) {
            var oTable = $('#' + $(this).attr('aria-controls'));
            oTable.dataTable();
            var searchField = $(this);
            var character = String.fromCharCode(e.keyCode);
            var isWordCharacter = character.match(/\w/);
            var isBackspaceOrDelete = (e.keyCode == 8 || e.keyCode == 46);
            // Trigger only on word characters, backspace or delete
            if (isWordCharacter || isBackspaceOrDelete) {
                clearTimeout(searchWaitInterval);
                searchWaitInterval = setTimeout(function() {
                    oTable.fnFilter(searchField.val());
                }, 500);
            }
        }).focus();
    }
};

var defaultOptions = jQuery.extend({
    fnServerData: function(sSource, aoData, fnCallback) {
        // Transform the request data to a dictionary
        var dataTablesData = [];
        for (var i = 0; i < aoData.length; i++) {
            dataTablesData[aoData[i].name] = aoData[i].value;
        }
        // Parse the columns names
        var columns = dataTablesData['sColumns'].split(',');
        // Define the request parameters to be sent
        var parameters = {
            'code[orlike]': dataTablesData['sSearch'].toUpperCase(),
            'reference_number[orlike]': dataTablesData['sSearch'],
            sort_field: columns[dataTablesData['iSortCol_0']],
            sort_direction: dataTablesData['sSortDir_0'],
            result_fields: dataTablesData['sColumns'],
            display_start: dataTablesData['iDisplayStart'],
            display_length: (dataTablesData['iDisplayLength'] != -1) ? dataTablesData['iDisplayLength'] : null,
        };
        // Enable loading state
        loadingModal.show();
        $('html, body').animate({ scrollTop: $("header").offset().top }, 200);
        // Call the API
        api.call({
            url: sSource,
            data: parameters,
            success: function(data, textStatus, jqXHR) {
                // Convert to format that DataTables understands
                var result = {};
                result.sEcho = dataTablesData['sEcho'];
                result.aaData = data.results;
                result.iTotalRecords = data.total;
                result.iTotalDisplayRecords = data.total; // data.filtered won't work here cause it considers pagination
                // Return to DataTables
                loadingModal.hide();
                fnCallback(result);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                loadingModal.hide();
            }
        }, true);
    }
}, baseOptions);

var userOptions = jQuery.extend({
    fnServerData: function(sSource, aoData, fnCallback) {
        // Transform the request data to a dictionary
        var dataTablesData = [];
        for (var i = 0; i < aoData.length; i++) {
            dataTablesData[aoData[i].name] = aoData[i].value;
        }
        // Parse the columns names
        var columns = dataTablesData['sColumns'].split(',');
        // Define the request parameters to be sent
        var parameters = {
            'name[like]': dataTablesData['sSearch'].toUpperCase(),
            sort_field: columns[dataTablesData['iSortCol_0']],
            sort_direction: dataTablesData['sSortDir_0'],
            result_fields: dataTablesData['sColumns'],
            display_start: dataTablesData['iDisplayStart'],
            display_length: (dataTablesData['iDisplayLength'] != -1) ? dataTablesData['iDisplayLength'] : null,
        };
        // Enable loading state
        loadingModal.show();
        $('html, body').animate({ scrollTop: $("header").offset().top }, 200);
        // Call the API
        api.call({
            url: sSource,
            data: parameters,
            type: "GET",
            success: function(data, textStatus, jqXHR) {
                // Convert to format that DataTables understands
                var result = {};
                result.sEcho = dataTablesData['sEcho'];
                result.aaData = data.results;
                result.iTotalRecords = data.total;
                result.iTotalDisplayRecords = data.total; // data.filtered won't work here cause it considers pagination
                // Return to DataTables
                loadingModal.hide();
                fnCallback(result);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                loadingModal.hide();
            }
        }, true);
    }
}, baseOptions)