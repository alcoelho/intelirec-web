var langs = ['en', 'pt-BR','es'];
var langCode = I18N;
var langJS = null;


var call_translate = function(jsdata) {

	langJS = jsdata;

	$("[tkey]").each(function(index) {
		var strTr = jsdata[$(this).attr('tkey')];

		var obj = '"'+$(this).attr('tkey')+'":"'+$(this).attr('tkey')+'"';

		localStorage.translatelist = localStorage.translatelist != undefined ? localStorage.translatelist +','+ obj : obj;

		$(this).html(strTr);
	});
}


langCode = langCode == '' ? navigator.language.substr(0, 2) : langCode;

if (langs.includes(langCode))
	$.getJSON(URI_JS + 'helpers/lang/' + langCode + '.json', call_translate);
else
	$.getJSON(URI_JS + 'helpers/lang/en.json', call_translate);


function translate(key, text) {

	//var obj = '"'+key+'":"'+text+'"';

	//localStorage.translatelist = localStorage.translatelist != undefined ? localStorage.translatelist +','+ obj : obj;

	var strTr = (langJS != null && langJS[key.toLowerCase()] != undefined) ? langJS[key.toLowerCase()] : text;
	
	return strTr;
}