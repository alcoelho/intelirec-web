var _global_excluded_status = [];

var _global_possibles_status = ['VERIFIED','SCRAP','OPEN','AVAILABLE','APPLIED', 'SENT','TO_SEND', 'DAMAGED_TO_REPAIR', 'REPAIRED', 'DAMAGED_TO_DESTROY', 'RECEIVED', 'REPLACED','DESTROYED'];

var _global_possibles_user_roles = ['SUPERADMIN', 'USER','FACTORY','ADMIN','WAREHOUSE','TRANSPORT','LAB','CONTRACTOR'];

var _global_possibles_types = ['BOX','BAG','SEAL','METER_BOX','METER_PALLET','METER'];
