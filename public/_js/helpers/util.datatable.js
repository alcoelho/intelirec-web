var dtDefaultOptions = {
  serverSide: true,
  lengthChange: true,
  paging: true,
  pagingType: "full_numbers",
  pageLength: 10,
  autoWidth: true,
  lengthMenu: [
    [10, 25, 50, 100],
    [10, 25, 50, 100]
  ],
  dom: 'flrtip',
  initComplete: function(settings, json) {
    $(".dt-buttons > button").removeClass("dt-button").addClass("mdc-button");
  },
  language: {
    "url": I18N == '' ? (URI_JS + 'helpers/lang/' + navigator.language.substr(0, 2) + '.json') : (URI_JS + 'helpers/lang/' + I18N + '.json')
  },
}


function setInputMaterial(label) {
  $('.dataTables_filter').addClass('mdc-text-field');
  $('.dataTables_filter').find('label').append('<label for="filter" class="mdc-text-field__label">'+label+'</label>');
  $($('.dataTables_filter').find('input')).addClass('mdc-text-field__input');
  $($('.dataTables_filter').find('input')).addClass('width-250');
  $($('.dataTables_filter').find('input')).after('<div class="mdc-line-ripple"></div');
  new mdc.textField.MDCTextField($('.dataTables_filter.mdc-text-field')[0]);
}