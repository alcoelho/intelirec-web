function getStorageFields(storage, status, type, divInformation, divGalery, callback) {

    var storageFields = [];

    //var sch = getFieldsSchemaByStatus(status);

    var st = storage[status];

    $.getJSON(URI_JS + 'helpers/schemas/' + type.toUpperCase() + '.json', function(data) {

        var sch = data[status];

        if (sch != undefined) {

            $.each(sch, function(key, item) {

                console.log(key);

                switch (item.type) {
                    case "list_group":
                        var listgroup = {
                            id: key,
                            items: [],
                            type: item.type,
                            title: translate(key, item.title) 
                        };

                        var count = 0;
                        var stop = false;

                        while (!stop) {

                            var its = [];

                            $.each(item.items, function(key, it) {

                                var id = key.replace('[i]', count);

                                if (st[id] != undefined) {

                                    var i = {
                                        id: id,
                                        type: it.type,
                                        title: translate(key, it.title) ,
                                        value: st[id]
                                    };

                                    its.push(i);

                                } else stop = true;

                            });

                            if (its.length > 0)
                                listgroup.items.push(its);

                            count++;
                        }

                        //if (listItens.values.length > 0)
                        storageFields.push(listgroup);
                        break;

                    case "image_group":

                        var count = 0;
                        var stop = false;

                        while (!stop) {

                            var id = key.replace('[i]', count);

                            if (st[id] != undefined) {

                                var i = {
                                    id: id,
                                    type: 'image',
                                    title: item.title + ' ' + (count + 1),
                                    value: st[id]
                                };

                                storageFields.push(i);

                                count++;

                            } else stop = true;
                        }

                        break;

                    case "date":
                        if (st[key] !== undefined) {
                            var obj = item;
                            obj.title =  translate(key, obj.title);
                            obj.value = getDateFromUTF8(st[key]);
                            storageFields.push(obj);
                        }
                        break;

                    default:
                        if (st[key] !== undefined) {
                            var obj = item;
                            obj.title =  translate(key, obj.title);
                            obj.value = st[key];
                            storageFields.push(obj);
                        }
                        break;
                }

            });

            callback(storageFields, divInformation, divGalery);
        }

    });

}


function renderStorageFieldsModal(fields, divInformation, divGalery) {

    if (fields != null) {

        var html = '<ul class"storage_fields">';

        var images = [];

        fields.forEach(function(item, position, array) {

            switch (item.type) {

                case "list_group":

                    var rows = [];
                    var titles = [];

                    item.items.forEach(function(it, position, array) {

                        var id_row = 0;
                        var coluns = [];

                        it.forEach(function(i, position, array) {

                            switch (i.type) {
                                case 'id':
                                    id_row = [i.value];
                                    break;

                                default:
                                    if (titles.length < (it.length - 1)) {
                                        titles.push(i.title);
                                    }
                                    coluns.push(i.value);
                                    break;
                            }
                        });

                        rows.push({
                            id: id_row,
                            coluns: coluns
                        });
                    });


                    html += '<li>';

                    html += '<h5>' + item.title + '</h5>';

                    html += '<table class="mdl-data-table dataTable no-footer">';

                    html += '<thead><tr>';

                    titles.forEach(function(title, position, array) {

                        html += '<th>' + title + '</th>';

                    });

                    html += '</tr></thead>';

                    html += '<tbody>';

                    rows.forEach(function(row, position, array) {

                        html += '<tr id="' + row.id + '">';

                        row.coluns.forEach(function(colum, position, array) {
                            html += '<td>' + colum + '</td>';
                        });

                        html += '</tr>';
                    });

                    html += '</tbody>';

                    html += '</table>';

                    html += '</li>';

                    break;

                case "boolean":
                    html += '<li><label class="title">' + item.title + ' : </label> ' + getIconBoolean(item.value) + '</li>';
                    break;
                case "text":
                console.log(item);
                    html += '<li><label class="title">' + item.title + ' : </label><span> ' + item.value + '</span></li>';
                    break;
                case "date":
                    html += '<li><label class="title">' + item.title + ' : </label><span> ' + item.value + '</span></li>';
                    break;
                case "image":
                    images.push({
                        title: translate(item.title, item.title),
                        src: item.value
                    });
                    break;
                case "number":
                    html += '<li><label class="title">' + item.title + ' : </label><span> ' + item.value + '</span></li>';
                    break;
            }

        });

        html += '</ul>';


        if (images.length > 0) {

            var htmlImages = '';

            for (i = 0; i < images.length; i++) {
                
                var imageSrcComplete = images[i].src +'?token='+ localStorage.token;
                
                htmlImages += '<a href="#">';
                htmlImages += '<img src="' + imageSrcComplete + '" alt="' + images[i].title + '" data-image="' + imageSrcComplete + '" data-description="' + images[i].title + '" style="display:none"/>';
                htmlImages += '</a>';
            }

            $('#' + divGalery).html(htmlImages);


            $('#' + divGalery).unitegallery({
                tile_enable_textpanel: true,
                tile_textpanel_title_text_align: "center",
                tile_textpanel_always_on: true, 
                gallery_width:900,

            });

        }

        else { $('#' + divGalery).html(('<center>'+ translate('no_data','There is no data to show') + '</center>')); }


        html = html == '<ul class"storage_fields"></ul>' ?  ('<center>'+ translate('no_data','There is no data to show') + '</center>') : html;

        $('#' + divInformation).html(html);
    }

}

function getIconBoolean(value) {
    if (value == 'true') {
        return '<i class="material-icons boolean-icon yes">thumb_up</i>'
    } else {
        return '<i class="material-icons boolean-icon no">thumb_down</i>'
    }
}


function getAssetCode(item)
{
    var code = {};

    code.id = item.id;
    code.huru = item.code;
    code.show = item.code;

    if(item.metadata_storage != undefined && item.metadata_storage.legacy_code != undefined && item.metadata_storage.legacy_code != '{}' && item.metadata_storage.legacy_code != '')
    {
        code.show = item.metadata_storage.legacy_code;
    }
    else if(item.metadata != undefined && item.metadata.legacy_code != undefined && item.metadata.legacy_code != '{}' && item.metadata.legacy_code != '')
    {
        code.show = item.metadata.legacy_code;
    }

    return code;
}