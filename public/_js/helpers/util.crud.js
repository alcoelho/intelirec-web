//FUNCTIONS FOR HELP CRUD FORM'S

var inputs = $('.mdc-text-field');
var action = getURLParameter('act');
var id = getURLParameter('id');


$(function() {

	if (inputs != null) {
		for (i = 0; i < inputs.length; i++) {
			new mdc.textField.MDCTextField(inputs[i]);
		}
	}

	setTimeout(function(){ loadFormDefault(action,false); }, 0);

	/*var selects = $('.mdc-select')
	if (selects != null) {

	    for (i = 0; i < selects.length; i++) {
	        const select = new mdc.select.MDCSelect(selects[i]);
	        select.listen('MDCSelect:change', () => {
	            alert(`Selected "${select.selectedOptions[0].textContent}" at index ${select.selectedIndex} ` +
	    `with value "${select.value}"`);
	        });
	    }
	}*/

});

function loadFormDefault(action, noedit) {

	verifyAction(action);

	$("#save").attr('action', action);
	switch (action) {
		case "PUT":
			labelForEdit();
			$('.details-only').hide();
			break;
		case "GET":
			if (noedit) {
				$('#save').hide();
			} else {
				
				$("#save").on("click", function(e) {
					e.preventDefault();
					window.location.href = window.location.href.replace('GET', 'PUT');
				});

				$('#save').html(translate('edit', 'EDIT'));
			}

			$('#cancel').html(translate('back', 'Back'));

			labelForEdit();
			disabledAllInputsForm();
			break;
		case "POST":
			loadingModal.show();
			$('.details-only').hide();
			loadForm(null);
			loadingModal.hide();
			break;
		default:
			break;
	}


}


function getModel(model) {

	if (action != 'POST') {

		loadingModal.show();

		api.call({
			url: '/' + model + '/' + id,
			type: 'GET',
			handleError: true,
			success: function(data, textStatus, jqXHR) {
				loadForm(data);
				loadingModal.hide();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//return process([]);
				loadingModal.hide();
			}
		}, true);

	}
}


function submitForm(id, model, action, url, redirect) {

	var red = redirect != null ? redirect : '../';

	console.log(redirect);

	verifyRequiredFields(model);

	loadingModal.show();
	api.call({
		url: action == 'PUT' ? url + '/' + id : url,
		type: action,
		data: model,
		handleError: true,
		success: function(data, textStatus, jqXHR) {
			loadingModal.hide();
			showMsgSucess();
			setTimeout(function() {
				window.location.replace(red);
			}, 500);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			loadingModal.hide();
		}
	}, true);
}


function verifyRequiredFields(model) {

	try {

		var listErros = '';

		$.each(model, function(index, value) {

			var required = $('#' + index)[0] != undefined && $('#' + index)[0].required != undefined ? $('#' + index)[0].required : $('#' + index)[0] != undefined && $('#' + index)[0].attributes['required'] != undefined;

			if (required && value == '') {
				listErros += translate(index, index) + ' ' + translate('is_required', 'Is required') + '<br>';
			}

		});

		if (listErros != '')
			throw listErros;
	} catch (e) {
		ErrorHandler.openErrorModal(false, e);
		throw new Error(e);
	}
}


function verifyAction(action) {
	if (action != 'POST' && action != 'PUT' && action != 'GET') {
		window.location.replace('../');
	}
}


function disabledAllInputsForm() {

	var inputs = $('form').find('input');
	var textAreas = $('form').find('textarea');

	$('.mdc-select').attr('aria-disabled', true);

	$('.mdc-line-ripple').addClass('disabled');

	for (i = 0; i < inputs.length; i++) {
		inputs[i].disabled = true;
		inputs[i].classList.add('disabled');
	}

	for (i = 0; i < textAreas.length; i++) {
		textAreas[i].disabled = true;
		textAreas[i].classList.add('disabled');
	}

}


function labelForEdit() {
	var labels = $('.mdc-text-field').find('label');

	if (labels != null) {

		for (i = 0; i < labels.length; i++) {
			labels[i].setAttribute('class', 'mdc-text-field__label mdc-text-field__label--float-above');
		}
	}

}