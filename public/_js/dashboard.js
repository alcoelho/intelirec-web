medicamentoAutoComplete("#medicamento");
var receitas = [];
var receitaSelected = 0;

Blockchain.init();

function auxSetFilter(filter, key, value) {
    if (value != null && value != "" && value != undefined) {
        filter.push("filter%5Bwhere%5D%5B" + key + "%5D=" + value);
    }
}

function getMedicamento(event) {
    event.preventDefault();
    $("#tab_receitas tbody").html("");
    //rescue filters
    //var medicamento = $("#medicamento").val();
    //var classe = $("#classe").val();

    var paciente_cpf = $("#paciente_cpf").val();

    //var data_inicio = $("#data_inicio").val();
    //var data_fim = $("#data_fim").val();

    var id = $("#id_receita").val();

    /*if (data_inicio > data_fim) {
        var msg = "Data inicial não pode ser maior que data final";
        showMsg(msg, "#msg_error", true, true);
    }*/

    var filter = [];
    //auxSetFilter(filter, "medicamento", medicamento);
    auxSetFilter(filter, "paciente_cpf", paciente_cpf);
    auxSetFilter(filter, "id", id);

    api.call({
        url: "/receitas?" + filter.join("&"),
        type: 'GET',
        handleError: true,
        success: function (data, textStatus, jqXHR) {
            getMeciamentosCallback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    }, true);
}

function getMeciamentosCallback(data) {
    for (i = 0; i < data.length; i++) {
        receitas[data[i].id] = data[i];
        var items = [];
        items.push("<td><span>" + data[i].id + "</span></td>");
        items.push("<td><span>" + data[i].paciente_nome + "</span></td>");
        items.push("<td><span>" + data[i].paciente_cpf + "</span></td>");
        items.push("<td><span>" + getDateHourFromUTF8(data[i].data) + "</span></td>");
        items.push("<td><span>" + getStatus(data[i].status) + "</span></td>");
        items.push("<td><a href=\"javascript:showReceitaDetail('" + data[i].id + "')\"><i class='icon-eye'></a></td>");
        $("<tr/>", {
            html: items.join("")
        }).appendTo("#tab_receitas tbody");
        items = [];
    }
}

function showReceitaDetail(id) {
    receitaSelected = id;
    $("#receita_detalhes_texto").html("")
    var receita = receitas[id];

    $('<input>').attr({
        type: 'hidden',
        id: 'receitaId',
        name: 'receitaId',
        value: receita.id
    }).appendTo('form');

    var items = [];
    $.each(receita, function (key, val) {

        if (key == "medicamentos") {
            var json = $.parseJSON('[' + val + ']');
            var medicamentosHtml = "";
            for (i = 0; i < json.length; i++) {
                var ul = "<ul><li>Nome: " + json[i].nome + "</li>" +
                    "<li>Instruções: " + json[i].instrucoes + "</li>" +
                    "<li>Codigo: " + json[i].cod + "</li>" +
                    "</ul>";
                medicamentosHtml = medicamentosHtml + ul;
            }
            items.push("<tr><td colspan='2'><h4>Medicamentos</h4>" + medicamentosHtml + "</td></tr>");

        } else {
            items.push("<tr><td><strong style='text-transform: uppercase;'>" + translate(key) + "</strong></td><td>" + val + "</td></tr>");
        }

    });
    $("<table>", {
        class: "table table-responsive-sm table-hover table-outline mb-0",
        html: items.join("")
    }).appendTo("#receita_detalhes_texto");

    if (receita.status != "Prescribed") {
        $("#btn_utilizar").hide();
        $("#btn_rejeitar").hide();
    }
    else {
        $("#btn_utilizar").show();
        $("#btn_rejeitar").show();
    }

    $("#receitaDetail").modal();
}



function updateReceita(status) {
    var receita = receitas[receitaSelected];
    receita.status = status;

    // if (receita.status == 'Processed') {
    //     Blockchain
    //         .process(receita.id)
    //         .then(function (ret) {
    //             console.log(ret);
    //         })
    //         .catch(function (err) {
    //             console.error(err);
    //         });
    // }

    // api.call({
    //     url: "/receitas/"+receitaSelected,
    //     type: 'PATCH',
    //     headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json'
    //     },
    //     data : JSON.stringify(receita),
    //     dataType: 'json',
    //     handleError: true,
    //     success: function(data, textStatus, jqXHR) {
    //         $('#receitaDetail').modal('toggle');
    //         showMsg("Receita atualizada com sucesso", 'SUCCESS');
    //     },
    //     error: function(jqXHR, textStatus, errorThrown) {
    //         showMsg("Erro ao atualizar receita", 'ERROR');
    //     }
    // }, true);

}

function clear() {
    $("#tab_receitas tbody").html("");
    $("#medicamento").val("");
    $("#classe").val("");
    $("#paciente_cpf").val("");
    $("#data_inicio").val("");
    $("#data_fim").val("");
    $("#id_receita").val("");
}