medicamentoAutoComplete("#medicamento");
var receitas = [];
var receitaSelected = 0;

Blockchain.init();

function auxSetFilter(filter, key, value) {
    if (value != null && value != "" && value != undefined) {
        filter.push("filter%5Bwhere%5D%5B" + key + "%5D=" + value);
    }
}

function clearAll() {
    $("#tab_regras tbody").html("");
    $('#regra_type').val("");
    $('#regra_type').prop('selectedIndex', 0);
    $('#regra_type option:first').prop('selected', true);
}

function getRegras(event) {
    event.preventDefault();

    $("#tab_regras tbody").html("");

    var regra_type = $("#regra_type").val();
    var filter = [];

    if ($("#regra_type").val() != '')
        auxSetFilter(filter, "regra_type", regra_type);

    api.call({
        url: "/regras?" + filter.join("&"),
        type: 'GET',
        handleError: true,
        success: function (data, textStatus, jqXHR) {
            getRegrasCallback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) { }
    }, true);
}

function getRegrasCallback(data) {
    for (i = 0; i < data.length; i++) {
        var items = [];
        items.push("<td><span>" + getRegraType(data[i].regra_type) + "</span></td>");
        items.push("<td><span>" + data[i].medico + " - " + data[i].crm + "</span></td>");
        items.push("<td><span>" + data[i].medicamento + "</span></td>");
        items.push("<td><a href=\"javascript:deleteRegra(" + data[i].id + ',' + data[i].medicamento_id + ")\"><i class='icon-trash'></a></td>");
        $("<tr/>", {
            html: items.join("")
        }).appendTo("#tab_regras tbody");
        items = [];
    }
}


function deleteRegra(id, medId) {

    Blockchain
        .removeProhibitedMedicine(medId)
        .then(function (ret) {
            console.log(ret);
            api.call({
                url: "/regras/" + id,
                type: 'DELETE',
                handleError: true,
                success: function (data, textStatus, jqXHR) {
                    showMsg("Regra removida com sucesso", 'SUCCESS');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    showMsg("Erro ao remover a regra", 'ERROR');
                }
            }, true);
        })
        .catch(function (err) {
            console.error(err);
            showMsg("Ocorreu um erro na Blockchain", 'ERROR');
        });
}