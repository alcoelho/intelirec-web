Blockchain = {
	web3Provider: null,
	contracts: {},
	init: function () {
		if (typeof web3 !== 'undefined') {
			Blockchain.web3Provider = web3.currentProvider;
		} else {
			Blockchain.web3Provider = new Web3.providers.HttpProvider('http://23.96.87.93:8545');
		}
		web3 = new Web3(Blockchain.web3Provider);
		return Blockchain.initContract();
	},

	initContract: function () {
		$.getJSON("http://23.96.87.93:8080/contract/PrescriptionStore.json", function (data) {
			var PrescriptionStoreArtifact = data;
			console.log(data);
			Blockchain.contracts.PrescriptionStore = TruffleContract(PrescriptionStoreArtifact);
			Blockchain.contracts.PrescriptionStore.setProvider(Blockchain.web3Provider);
		});
	},

	handlePrescribe: function (event) {
		web3.eth.getAccounts(function (error, accounts) {
			if (error) {
				console.log(error);
			}

			var account = accounts[0];
			// web3.personal.unlockAccount(account, "doctor");
			var uuid = lil.uuid().slice(0, -4);
			console.log(uuid);

			Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
				prescriptionInstance = instance;
				return prescriptionInstance.prescribe('Lula Molusco', '1234', 'patient', '123', '123', 'Abrantes', uuid, moment().unix(), { from: account, gas: 3000000 });
			}).then(function (result) {
				console.log(JSON.stringify(result));
			}).catch(function (err) {
				console.error(err);
			});
		});
	},

	prescribe: function (docName, crm, patName, cpf, rg, home) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[1];
				// web3.personal.unlockAccount(account, "doctor");
				var uuid = lil.uuid().slice(0, -4);
				console.log('Id da receita gerada: ' + uuid);

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.prescribe(docName, crm, patName, cpf, rg, home, uuid, moment().unix(), { from: account, gas: 3000000 });
				}).then(function (result) {
					var precription = {
						id: uuid,
						hash: JSON.parse(JSON.stringify(result)).receipt.transactionHash
					};
					resolve(precription);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	addMedicine: function (id, instructions, prescId) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[1];
				// web3.personal.unlockAccount(account, "doctor");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.addMedicineToPrescription(prescId, id, instructions, { from: account, gas: 3000000 });
				}).then(function (result) {
					console.log(result);
					resolve(result);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	addProhibitedMedicine: function (id) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[0];
				console.log(id);
				// web3.personal.unlockAccount(account, "anvisa");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.addProhibitedMedicine(id, moment().unix(), { from: account, gas: 3000000 });
				}).then(function (result) {
					console.log(result);
					var hash = JSON.parse(JSON.stringify(result)).receipt.transactionHash;
					resolve(hash);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	removeProhibitedMedicine: function (id) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[0];
				console.log(id);
				// web3.personal.unlockAccount(account, "anvisa");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.removeProhibitedMedicine(id, { from: account, gas: 3000000 });
				}).then(function (result) {
					console.log(result);
					var hash = JSON.parse(JSON.stringify(result)).receipt.transactionHash;
					resolve(hash);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	isProhibitedMedicine: function (id) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[1];
				console.log(id);
				// web3.personal.unlockAccount(account, "doctor");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.isProhibitedMedicine.call(id, { from: account, gas: 3000000 });
				}).then(function (result) {
					resolve(result);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	getMedicineByPrescriptionId: function (prescId) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[0];
				// web3.personal.unlockAccount(account, "doctor");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.getMedicinesByPrescription.call(prescId, { from: account, gas: 3000000 });
				}).then(function (result) {
					console.log(result);
					resolve(result);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},

	handleGetTotal: function (event) {
		web3.eth.getAccounts(function (error, accounts) {
			if (error) {
				console.log(error);
			}

			var account = accounts[0];

			Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
				prescriptionInstance = instance;
				return prescriptionInstance.getTotalStoredPrescriptions.call({ from: account, gas: 3000000 });
			}).then(function (result) {
				console.log(JSON.stringify(result));
			}).catch(function (err) {
				console.log(err.message);
			});
		});
	},

	handleGetOwner: function (event) {
		web3.eth.getAccounts(function (error, accounts) {
			if (error) {
				console.log(error);
			}

			var account = accounts[0];

			Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
				prescriptionInstance = instance;
				return prescriptionInstance.getOwner.call({ from: account });
			}).then(function (result) {
				console.log(JSON.stringify(result));
			}).catch(function (err) {
				console.log(err.message);
			});
		});
	},

	handleGet: function (prescId) {
		web3.eth.getAccounts(function (error, accounts) {
			if (error) {
				console.log(error);
			}

			var account = accounts[0];

			Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
				prescriptionInstance = instance;
				return prescriptionInstance.getPrescriptionById.call(prescId, { from: account, gas: 3000000 });
			}).then(function (result) {
				// console.log(JSON.parse(JSON.stringify(result))`]);
				// console.log(Blockchain.handleDatetime(JSON.parse(JSON.stringify(result))[1]));
				console.log(JSON.stringify(result));
			}).catch(function (err) {
				console.error(err.message);
			});
		});
	},

	updatePrescriptionState: function (prescId, pharmaName, crf, status) {
		return new Promise(function (resolve, reject) {
			web3.eth.getAccounts(function (error, accounts) {
				if (error) {
					console.log(error);
				}

				var account = accounts[2];
				// web3.personal.unlockAccount(account, "pharma");

				Blockchain.contracts.PrescriptionStore.deployed().then(function (instance) {
					prescriptionInstance = instance;
					return prescriptionInstance.updatePrescriptionState(prescId, pharmaName, crf, status, moment().unix(), { from: account, gas: 3000000 });
				}).then(function (result) {
					var transaction = {
						hash: JSON.parse(JSON.stringify(result)).receipt.transactionHash
					};
					console.log(JSON.stringify(result));
					resolve(transaction);
				}).catch(function (err) {
					reject(err);
				});
			});
		});
	},
};

// $(function () {
//     $(window).on('load', function () {
//         Blockchain.init();
//     });
// });
