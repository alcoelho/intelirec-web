$(function(e) {

    $('#login-button').on('click', function(e) {
        login(e);
    });

    function login(e) {
        e.preventDefault();

        var email = $("#email").val();
        var pass = $("#password").val();

        api.call({
            url: "/users?filter%5Bwhere%5D%5Bemail%5D=" + email + "&filter%5Bwhere%5D%5Bpass%5D=" + pass,
            type: 'GET',
            handleError: true,
            success: function(data, textStatus, jqXHR) {
                auth(data);
                //loadingModal.hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //return process([]);
                //loadingModal.hide();
            }
        }, true);
    }
    function auth(data) {
        if (data.length > 0) {
            localStorage.setItem("username", data[0].name);
            localStorage.setItem("clinic", data[0].name);
            localStorage.setItem("address", data[0].address);
            localStorage.setItem("crm", data[0].crm);
            localStorage.setItem("farma_cod", data[0].farma_cod);
            localStorage.setItem("crf", data[0].crf);
            localStorage.setItem("email", data[0].email);
            localStorage.setItem("profile", data[0].type);
            document.cookie = 'authorized=1; path=/';
            window.location.href = "../dashboard/";
        } else {
            showMsg("Usuário ou senha inválidos", 'ERROR');
        }
    }

});