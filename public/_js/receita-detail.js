medicamentoAutoComplete("#medicamento_medicamento", "#medicamento_cod");

var medicamentos = [];
var receita = {};
var indexEdit = null;

loadForm();
Blockchain.init();

function loadForm() {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth();
    var ano = data.getFullYear();
    var str_data = dia + '/' + (mes + 1) + '/' + ano;
    $("#medico_nome").val(localStorage.username);
    $("#medico_crm").val(localStorage.crm);
    $("#medico_endereco").val(localStorage.address);
    $("#data").val(str_data);
}


function auxListMedicamentos() {

    $("#tab_medicamentos tbody").remove();

    var itens = [];

    for (i = 0; i < medicamentos.length; i++) {
        itens.push("<tr>");
        itens.push("<td><span>" + medicamentos[i].nome + "</span></td>");
        itens.push("<td><span>" + medicamentos[i].instrucoes + "</span></td>");
        var actions = "<a style=\"margin:0 20px 0 0;\" href=\"javascript:editRemedio('" + i + "')\"><i class='icon-pencil'/></a>";
        actions = actions + "<a style=\"color:red;\" href=\"javascript:removeRemedio('" + i + "')\"><i class='icon-trash'/></a>";
        itens.push("<td>" + actions + "</td>");
        itens.push("</tr>");
    }

    $("<tbody/>", {
        html: itens.join("")
    }).appendTo("#tab_medicamentos");
    itens = [];
}

function auxClearAllFieldsMedi() {

    $("#tab_medicamentos tbody").remove();
    $("#medicamento_medicamento").val("");
    $("#medicamento_instrucoes").val("");
    $("#medicamento_cod").val("");
}

function auxClearAllFieldRec() {

    $("#medico_nome").val("");
    $("#medico_crm").val("");
    $("#medico_endereco").val("");
    $("#paciente_nome").val("");
    $("#paciente_cpf").val("");
    $("#paciente_rg").val("");
    $("#paciente_endereco").val("");

}

function addMedicamento() {
    var medicamento = $("#medicamento_medicamento").val();
    var medicmanento_instrucoes = $("#medicamento_instrucoes").val();
    var medicmanento_cod = $("#medicamento_cod").val();

    if (medicamento == "" || medicmanento_cod == "" || medicmanento_instrucoes == "") {
        var msg = "Campos obrigatórios não informados";
        showMsg(msg, "#error_msg", true, true);
    }

    var remed = {
        nome: medicamento,
        instrucoes: medicmanento_instrucoes,
        cod: medicmanento_cod
    }

    var exists = false;
    var verifyMedicine = new Promise(function (resolve, reject) {
        Blockchain
            .isProhibitedMedicine(remed.cod)
            .then(function (ret) {
                console.log(JSON.stringify(ret));
                resolve(JSON.stringify(ret));
            })
            .catch(function (err) {
                console.error(err);
                reject(false);
            });
    }).then(function(isProhibited) {
        if(isProhibited == 'true') {
            showMsg("Medicamento proíbido na Blockchain", 'ERROR');
            exists = true;
        }else {
            auxClearAllFieldsMedi();

        if (indexEdit != null) {
            medicamentos[indexEdit] = remed;
            $("#medicamento_medicamento").prop("disabled", false);
            $("#btnAddMed").html("Adicionar");
            $("#btnCancel").hide();
            indexEdit = null;
    
        } else {
            medicamentos.push(remed);
        }
    
        auxListMedicamentos();
    
        showMsg("Medicamento adicionado", "#msg_sucess", false, false);
        }
    }).catch(function(err) {
        auxClearAllFieldsMedi();

        if (indexEdit != null) {
            medicamentos[indexEdit] = remed;
            $("#medicamento_medicamento").prop("disabled", false);
            $("#btnAddMed").html("Adicionar");
            $("#btnCancel").hide();
            indexEdit = null;
    
        } else {
            medicamentos.push(remed);
        }
    
        auxListMedicamentos();
    
        showMsg("Medicamento adicionado", "#msg_sucess", false, false);
    });
}


function editRemedio(index) {

    indexEdit = index;
    var remed = medicamentos[index];
    $("#medicamento_medicamento").val(remed.nome);
    $("#medicamento_medicamento").prop("disabled", true);
    $("#medicamento_instrucoes").val(remed.instrucoes);
    $("#medicamento_cod").val(remed.cod);
    $("#btnAddMed").html("Salvar");
    $("#btnCancel").show();

}

function removeRemedio(index) {
    medicamentos.splice(index);
    auxListMedicamentos();
}

function saveReceita(event) {
    event.preventDefault();

    var receita = {};

    receita.data = new Date().toISOString();
    receita.medico_nome = $("#medico_nome").val();
    receita.medico_crm = $("#medico_crm").val();
    //receita.medicoEndereco = $("#medico_endereco").val();
    receita.paciente_nome = $("#paciente_nome").val();
    receita.paciente_cpf = $("#paciente_cpf").val();
    receita.paciente_rg = $("#paciente_rg").val();
    receita.paciente_address = $("#paciente_endereco").val();
    receita.status = "Prescribed";

    var medicamentosJson = [];

    for (i = 0; i < medicamentos.length; i++) {
        medicamentosJson.push(JSON.stringify(medicamentos[i]));
    }

    receita.medicamentos = medicamentosJson.join(",");

    var RegExp = /["]/g;
    receita.medicamentos = receita.medicamentos.replace(RegExp, '"');

    var prescribeAsync = new Promise(function (resolve, reject) {
        var prescriptionReturn = {
            id: "",
            hash: ""
        }
        Blockchain.prescribe(receita.medico_nome,
            receita.medico_crm,
            receita.paciente_nome,
            receita.paciente_cpf,
            receita.paciente_rg,
            receita.paciente_address)
            .then(function (prescription) {
                console.log(prescription.hash);
                hashTransaction = prescription.hash;
                for (i = 0; i < medicamentos.length; i++) {
                    Blockchain
                        .addMedicine(medicamentos[i].cod, medicamentos[i].instrucoes, prescription.id)
                        .then(function (ret) {
                            console.log(JSON.stringify(ret));
                        })
                        .catch(function (err) {
                            console.error(err);
                            reject(err);
                        });
                }
                prescriptionReturn.id = prescription.id;
                prescriptionReturn.hash = hashTransaction;
                resolve(prescriptionReturn);
            }).catch(function (err) {
                console.error(err);
                reject(err);
            });
    }).then(function (prescriptionReturn) {
        console.log('BLOCKCHAIN OK');
        console.log(prescriptionReturn);
        receita.blockchain_hash = prescriptionReturn.hash;
        receita.id = prescriptionReturn.id;
        api.call({
            url: "/receitas",
            type: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(receita),
            dataType: 'json',
            handleError: true,
            success: function (data, textStatus, jqXHR) {
                auxClearAllFieldsMedi();
                auxClearAllFieldRec();
                loadForm();
                medicamentos = [];
                receita = {};
                indexEdit = null;
                showMsg("Receita registrada com sucesso", 'SUCCESS');
                setTimeout(function(){ window.location.href = "../index.php"; }, 5000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showMsg("Erro ao registrar receita", 'ERROR');
            }
        }, true);
    }).catch(function (err) {
        console.error(err);
        showMsg("Erro ao registrar receita na Blockchain", 'ERROR');
    });


}