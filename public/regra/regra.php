<div class="card">
  
  <div class="card-body">
    <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;"></div>
    <form onsubmit="getRegras(event)">
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_receita">Tipo de regra</label>
          <select class="custom-select" id="regra_type">
            <option value="" selected>Selecione...</option>
            <option value="REMED_PROIBIDO">Remedio proibido</option>
            <option value="CRM_SUSP">CRM Suspenso</option>
          </select>
        </div>
      </div>
      <div class="form-actions" style="margin:0 0 2% 0">
        <button class="btn btn-primary" type="submit">Filtrar</button>
        <a class="btn btn-secondary" type="button" href="javascript:clear()">Limpar</a>
      </div>
    </form>
    <div class="row" id="hist_table">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Regras</div>
          <div class="card-body">
            <br>
            <table class="table table-responsive-sm table-hover table-outline mb-0" id="tab_regras">
              <thead class="thead-light">
                <tr>
                  <th>Regra</th>
                  <th>Médico</th>
                  <th>Medicamento</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-->
    </div>
    
  </div>
</div>