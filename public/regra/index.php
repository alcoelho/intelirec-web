<?php
require_once(dirname(__FILE__) . '/../../application/config/global.php');
require_once(PATH_HELPERS . 'TemplateHandler.php');
if(!isset($_COOKIE['authorized'])){
    header("location:" . URI_ROOT);
}

echo "<script type='text/javascript'>
	var PAGE_PERMISSION = 'ANVISA';
    </script>";

$template = new TemplateHandler();

$template->set_page_title('<span>Regras</span>');

$template->set_css_files(array('regra.css'));
$template->set_js_head_files(array());
$template->set_js_files(array(
    'regra.js'
));
$template->set_content_file('regra/regra.php');
$template->show();