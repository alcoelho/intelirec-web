<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;"></div>
        <div class="alert alert-success" role="alert" id="msg_sucess_top" style="display: none;"></div>
        <form onsubmit="saveRegra(event)">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="id_receita">Tipo de regra</label>
              <select class="custom-select" id="regra_type">
                <option value="" selected>Selecione...</option>
                <option value="REMED_PROIBIDO">Remedio proibido</option>
                <option value="CRM_SUSP">CRM Suspenso</option>
              </select>
            </div>
          </div>
          <div class="row" id="dados_medico" style="display: none;">
            <div class="form-group col-sm-4">
              <label for="medico_crm">CRM</label>
              <input class="form-control" id="medico_crm" type="text">
            </div>
            <div class="form-group col-sm-8">
              <label for="medico_nome">Nome</label>
              <input class="form-control" id="medico_nome" type="text">
            </div>
          </div>
          <div class="row" id="dados_medicamento" style="display: none;">
            <div class="form-group col-sm-12">
              <label for="medicamento_medicamento">Medicamento</label>
              <input class="form-control" id="medicamento_medicamento" type="text" placeholder="Medicamento">
            </div>
            <input type="hidden" id="medicamento_cod" value="">
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Salvar</button>
            <button class="btn btn-secondary" type="button">Cancelar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>