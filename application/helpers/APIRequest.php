<?php
class APIRequest {

    public $url;
    public $header;
    public $body;
    public $statusCode;
    private $curl;

    public function get($target, $parameters = null, $allowedCodes = array(200)) {
        // Set URL
        $this->url = $parameters ? sprintf('%s?%s', $target, http_build_query($parameters)) : $target;
        $this->url = URI_API . $this->url;
        // Setup request
        $this->setup();
        // Send request
        return $this->request($allowedCodes);
    }

    public function post($target, $parameters = null, $allowedCodes = array(200, 201)) {
        // Set URL
        $this->url = URI_API . $target;
        // Setup request
        $this->setup();
        // Add POST options
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $parameters);
        // Send request
        return $this->request($allowedCodes);
    }

    private function setup() {
        global $config;
        // Create a new cURL resource
        $this->curl = curl_init();
        // Set default options
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        curl_setopt($this->curl, CURLOPT_HEADER, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->curl, CURLOPT_USERPWD, $config['api_username'] . ':' . $config['api_password']);
    }

    private function request($allowedCodes) {
        // Execute request
        $response = curl_exec($this->curl);
        // Check for errors
        $errorNumber = curl_errno($this->curl);
        if ((int) $errorNumber != 0) {
            throw new RuntimeException(curl_error($this->curl), $errorNumber);
        }
        // Parse response
        $headerSize = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
        $contentType = curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE);
        $this->header = http_parse_headers(substr($response, 0, $headerSize));
        $this->body = substr($response, $headerSize);
        $this->statusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        // Close cURL resource, and free up system resources
        curl_close($this->curl);
        // Validate
        if (!in_array($this->statusCode, $allowedCodes)) {
            throw new Exception("Unexpected response: Request: GET {$this->url} - Response: {$this->statusCode} {$this->body}");
        }
        if ($contentType != 'application/json') {
            throw new Exception("Not JSON response: Request: GET {$this->url} - Response: {$this->statusCode} {$this->body}");
        }
        // Decode the response body
        $this->body = json_decode($this->body, true);
        // Return
        return $this->body;
    }
}