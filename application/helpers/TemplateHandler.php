<?php
require_once(dirname(__FILE__) . '/../config/constants.php');

class TemplateHandler {
    
    private $global_file;
    private $header_file;
    private $menu_file;
    private $body_file;
    private $footer_file;
    private $content_file;
    private $title;
    private $page_title;
    private $css_files;
    private $js_files;
    private $js_head_files;
    private $meta_tags;
    private $active_nav_item;
    private $authenticated_only;
    
    public function __construct() {
        $this->global_file = DEFAULT_GLOBAL_FILE;
        $this->header_file = DEFAULT_HEADER_FILE;
        $this->footer_file = DEFAULT_FOOTER_FILE;
        $this->menu_file = DEFAULT_MENU_FILE;
        $this->title = DEFAULT_TITLE;
        $this->page_title = DEFAULT_TITLE;
        $this->css_files = array();
        $this->js_files = array();
        $this->js_head_files = array();
        $this->meta_tags = array();
        $this->active_nav_item = 0;
        $this->authenticated_only = false;
    }
    
    public function show() {
        // if ($this->authenticated_only && !Auth::check()) {
        //  $this->body_file = DEFAULT_FORBIDDEN_FILE;
        // }
        if ($this->template_file_exists($this->global_file)) {
            include(PATH_TEMPLATES . $this->global_file);
        }
        else {
            throw new Exception("Global template not found.");
        }
    }
    
    public function show_header() {
        if ($this->template_file_exists($this->header_file)) {
            include(PATH_TEMPLATES . $this->header_file);
        }
    }

    public function show_menu() {
        if ($this->template_file_exists($this->menu_file)) {
            include(PATH_TEMPLATES . $this->menu_file);
        }
    }
    
    public function show_footer() {
        if ($this->template_file_exists($this->footer_file)) {
            include(PATH_TEMPLATES . $this->footer_file);
        }
    }
    
    public function show_content() {
        if ($this->public_file_exists($this->content_file)) {
            include(PATH_PUBLIC . $this->content_file);
        }
    }

    private function update_css_extension($name) {
        if (ENVIRONMENT_TYPE == ENVIRONMENT_PROD) {
            if (strpos($name, ".less") !== false) {
                $name = str_replace(".less", ".css", $name);
            }
        }
        return $name;
    }
        
    public function import_css() {
        if ($this->css_files) {
            $internal_css_files = array();
            foreach($this->css_files as $css_file) {
                $css_file = $this->update_css_extension($css_file);
                $rel = 'stylesheet';
                if (strrchr($css_file, '.') == '.less') {
                    $rel = 'stylesheet/less';
                }
                if ($this->is_external($css_file)) {
                    echo '<link rel="' . $rel . '" type="text/css" href="'. $css_file .'?ver='.APP_VERSION. '" />' . "\n";
                }
                else {
                    $internal_css_files[] = $css_file;
                    echo '<link rel="' . $rel . '" type="text/css" href="'. URI_CSS . $css_file .'?ver='.APP_VERSION. '" />' . "\n";
                }
            }
            if (ENVIRONMENT_TYPE == ENVIRONMENT_DEV) {
                $this->import_specific_js('libraries/less.min.js');
            }
        }
    }

    public function import_specific_js($js_file) {
        if ($this->is_external($js_file)) {
            if(strpos($js_file, '?') == false)
            {
                echo '<script type="text/javascript" src="' . $js_file .'?ver='.APP_VERSION. '" /></script>' . "\n";
            }
            else {
                echo '<script type="text/javascript" src="' . $js_file .'&ver='.APP_VERSION. '" /></script>' . "\n";
            }

        }
        else {
            $internal_js_files[] = $js_file;

            if(strpos($js_file, '?') == false)
            {
                echo '<script type="text/javascript" src="'. URI_JS . $js_file .'?ver='.APP_VERSION. '" /></script>' . "\n";            
            }
            else {
                    echo '<script type="text/javascript" src="'. URI_JS . $js_file .'&ver='.APP_VERSION. '" /></script>' . "\n";            
            }

           
        }
    }
    
    public function import_js() {
        if ($this->js_files) {
            $internal_js_files = array();
            foreach($this->js_files as $js_file) {
                $this->import_specific_js($js_file);
            }
        }
    }

    public function import_jsHead() {
        if ($this->js_head_files) {
            foreach($this->js_head_files as $js_file) {
                $this->import_specific_js($js_file);
            }
        }
    }
    
    public function print_meta_tags() {
        if ($this->meta_tags) {
            foreach($this->meta_tags as $meta_tag) {
                echo $meta_tag . "\n";
            }
        }
    }
        
    public function is_external($file) {
        if (substr($file, 0, 4) == 'http' || substr($file, 0, 3) == 'www') {
            return true;
        }
        return false;
    }
    
    private function template_file_exists($file) {
        if ($file) {
            return file_exists(PATH_TEMPLATES . $file);
        }
        return false;
    }
    
    private function public_file_exists($file) {
        if ($file) {
            return file_exists(PATH_PUBLIC . $file);
        }
        return false;
    }
    
    public function get_header_file() {
        return $this->header_file;
    }
    
    public function set_header_file($header_file) {
        if ($this->template_file_exists($header_file)) {
            $this->header_file = $header_file;
        }
        else {
            throw new Exception('Header file does not exists.');
        }
    }

    public function get_menu_file() {
        return $this->menu_file;
    }
    
    public function set_menu_file($menu_file) {
        if ($this->template_file_exists($menu_file)) {
            $this->menu_file = $menu_file;
        }
        else {
            throw new Exception("Menu file ($menu_file) does not exists.");
        }
    }
    
    public function get_footer_file() {
        return $this->footer_file;
    }
    
    public function set_footer_file($footer_file) {
        if ($this->template_file_exists($footer_file)) {
            $this->footer_file = $footer_file;
        }
        else {
            throw new Exception("Footer file ($footer_file) does not exists.");
        }
    }
    
    public function get_content_file() {
        return $this->content_file;
    }
    
    public function set_content_file($content_file) {
        if ($this->public_file_exists($content_file)) {
            $this->content_file = $content_file;
        }
        else {
            throw new Exception("Content file ($content_file) does not exists.");
        }
    }
    
    public function get_title() {
        return $this->title;
    }
    
    public function set_title($title) {
        $this->title = $title;
    }

    public function get_page_title() {
        return $this->page_title;
    }
    
    public function set_page_title($page_title) {
        $this->page_title = $page_title;
    }
    
    public function get_css_files() {
        return $this->css_files;
    }
    
    public function set_css_files($css_files) {
        if (is_array($css_files)) {
            $this->css_files = $css_files;
        }
        else {
            throw new Exception('CSS files must be an array.');
        }
    }
    
    public function get_js_files() {
        return $this->js_files;
    }
    
    public function set_js_files($js_files) {
        if (is_array($js_files)) {
            $this->js_files = $js_files;
        }
        else {
            throw new Exception('JS files must be an array.');
        }
    }


    public function get_js_head_files() {
        return $this->js_head_files;
    }
    
    public function set_js_head_files($js_head_files) {
        if (is_array($js_head_files)) {
            $this->js_head_files = $js_head_files;
        }
        else {
            throw new Exception('JS head files must be an array.');
        }
    }
    
    public function get_meta_tags() {
        return $this->meta_tags;
    }
    
    public function set_meta_tags($meta_tags) {
        if (is_array($meta_tags)) {
            $this->meta_tags = $meta_tags;
        }
        else {
            throw new Exception('Meta tags must be an array.');
        }
    }
    
    public function get_active_nav_item() {
        return $this->active_nav_item;
    }
    
    public function set_active_nav_item($active_nav_item) {
        if (is_numeric($active_nav_item)) {
            $this->active_nav_item = $active_nav_item;
        }
        else {
            throw new Exception('Active navigation item must be a numeric index.');
        }
    }
    
    public function is_authenticated_only() {
        return $this->authenticated_only;
    }
    
    public function set_authenticated_only($authenticated_only) {
        if (is_bool($authenticated_only)) {
            $this->authenticated_only = $authenticated_only;
        }
        else {
            throw new Exception('Authenticated only must be a boolean.');
        }
    }

    public function set_body_file($body_file) {
        if ($this->template_file_exists($body_file)) {
            $this->body_file = $body_file;
        }
        else {
            throw new Exception("Body file ($body_file) does not exists.");
        }
    }
}