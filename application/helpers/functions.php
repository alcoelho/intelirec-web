<?php
// require_once(dirname(__FILE__) . '/../config/global.php');
// require_once(PATH_LIBRARIES . 'swift/swift_required.php');

/**
 * Default exception handler for exceptions that were not caught within a try/catch block
 *
 * @param Exception $exception - The exception
 */
function handle_exception($exception) {
    global $config;
    $error = array();
    $error['message'] = $exception->getMessage();
    if ($trace = $exception->getTrace()) {
        $trace = $trace[0];
        $error['file'] = $exception->getFile();
        $error['line'] = isset($trace['line']) ? $trace['line'] : '';
        $error['class'] = isset($trace['class']) ? $trace['class'] : '';
        $error['function'] = isset($trace['function']) ? $trace['function'] : '';
        // $error['trace'] = $exception->getTrace();
        $error['trace'] = $exception->getTraceAsString();
    }
    if (get_class($exception) == 'LogException' && $exception->getAttachment()) {
        log_error($error, $exception->getAttachment());
    } else {
        log_error($error);
        if (!headers_sent()) {
            http_response_code(500);
        }
    }
    if (!$config['log_debug'] && $_SERVER['REQUEST_METHOD'] == 'GET') {
        server_error_redirect();
    }
    exit();
}

/**
 * Handle errors in a script
 *
 * @param integer $errno - The level of the error raised
 * @param string $errstr - The error message
 * @param string $errfile - The filename that the error was raised in
 * @param integer $errline - The line number the error was raised at
 */
function handle_error($errno, $errstr, $errfile, $errline) {
    throw new Exception($errstr, $errno);
}

/**
 * Redirect to a custom server error URL
 */
function server_error_redirect() {
    if (!headers_sent()) {
        http_response_code(500);
        header('Location: ' . URI_ERROR_SERVER);
        exit();
    } else {
        exit('Server error');
    }
}

/**
 * Print the error message (debug mode), save to a file and send it via email
 *
 * @param array/string $message - The error message
 */
function log_error($description, $attachment = null) {
    global $config;
    if ($config['log_enabled']) {
        // If DEBUG mode is enabled, print it
        if ($config['log_debug']) {
            if (is_array($description)) {
                // Format the error message
                print array_to_html_list($description);
            } else {
                print $description;
            }
        }
        // Build the log data
        $log = array(
            'date' => date('Y-m-d H:i:s'),
            'referer_uri' => (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''),
            'request_uri' => (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''),
            'user_ip' => (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''),
            'user_agent' => (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''),
            'description' => $description
        );
        // Log to a file
        if ($handle = fopen(LOGS_ERROR, 'w')) {
            fwrite($handle, json_encode($log) . "\n");
            fclose($handle);
        }
        // Send to email
        if ($config['log_email']) {
            if (is_array($description)) {
                // Format the error message
                $description = array_to_html_list($description);
            }
            // Define the content
            $message = '<h3>An error ocurred on ' . $log['date'] . '</h3>';
            $message.= '<br /><b>Referer URI:</b> ' . $log['referer_uri'];
            $message.= '<br /><b>Request URI:</b> ' . $log['request_uri'];
            $message.= '<br /><b>User IP:</b> ' . $log['user_ip'];
            $message.= '<br /><b>User Agent:</b> ' . $log['user_agent'];
            $message.= '<br /><b>Description:</b><br />' . $description;
            // Define the subject
            $subject = 'Error log (' . $log['date'] . ')';
            // Send the email
            send_mail_background($config['log_from'], $config['log_email'], $subject, $message, $attachment);
        }
    }
}

/**
 * Send an email with attachment
 *
 * @param string/array $from - Sender email address and name
 * @param string/array $to - Receiver email address and name
 * @param string $subject - Subject of the email to be sent
 * @param string $message - Message to be sent
 * @param string $attachment - File to be attached (optional)
 * @return boolean
 */
function send_mail($from, $to, $subject, $message, $attachment = null, $content_type = 'text/html') {
    global $config;
    try {
        // Create the SMTP Transport
        $transport = Swift_SmtpTransport::newInstance($config['smtp_host'], $config['smtp_port'])
            ->setUsername($config['smtp_username'])
            ->setPassword($config['smtp_password']);
        // Add encryption
        if ($config['smtp_port'] == 465 || $config['smtp_port'] == 587) {
            $transport->setEncryption('ssl');
        }
        // Create the Mailer using the created Transport
        $mailer = Swift_Mailer::newInstance($transport);
        // Create the message
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($message, $content_type);
        // Add attachment
        if ($attachment) {
            $file_info = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($file_info, $attachment);
            finfo_close($file_info);
            $message->attach(Swift_Attachment::fromPath($attachment, $mimetype));
        }
        // Send the message
        if (!$mailer->send($message)) {
            // Fallback to mail() Transport
            $transport = Swift_MailTransport::newInstance();
            // Create the Mailer using the created Transport
            $mailer = Swift_Mailer::newInstance($transport);
            // Try again
            return $mailer->send($message);
        } else {
            return true;
        }
    } catch (Exception $e) {
        return false;
    }
}

/**
 * Send an email with attachment in background
 *
 * @param string/array $from - Sender email address and name
 * @param string/array $to - Receiver email address and name
 * @param string $subject - Subject of the email to be sent
 * @param string $message - Message to be sent
 * @param string $attachment - File to be attached (optional)
 */
function send_mail_background($from, $to, $subject, $message, $attachment = null) {
    // Prepare the arguments
    $from_name = is_array($from) ? reset($from) : null;
    $to_name = is_array($to) ? reset($to) : null;
    $from = is_array($from) ? key($from) : $from;
    $to = is_array($to) ? key($to) : $to;
    // Write the message content to a file
    $message_file = tempnam(PATH_TEMPORARY, 'mail_');
    if ($handle = fopen($message_file, 'w')) {
        fwrite($handle, $message);
        fclose($handle);
        // Execute the script
        $command = '/usr/bin/env php ' . SCRIPT_SENDMAIL;
        shell_exec("$command '$from' '$to' '$subject' '$message_file' '$attachment' '$from_name' '$to_name' >> " . LOGS_MAIL . " &");
    }
}

/**
 * Format an array as a list (<ul> <li>)
 *
 * @param array $array - The input array (can be multi-dimensional)
 * @return string - The list
 */
function array_to_html_list($array, $level = 1) {
    $formated_message = '<ul>';
    foreach ($array as $key=>$value) {
        if (is_array($value) && $level < 5) {
            $formated_message.= '<ul><strong>' . ucwords($key) . '</strong>';
            $formated_message.= array_to_html_list($value, $level++);
            $formated_message.= '</ul>';
        } elseif (!is_object($value)) {
            $level = 1;
            $formated_message.= '<li><strong>' . ucwords($key) . '</strong>: ' . $value . '</li>';
        }
    }
    $formated_message.= '</ul>';
    return $formated_message;
}

/**
 * Get or Set the HTTP response code
 * 
 * @param int $code - The response code to be set
 * @return int - The current response code. By default the return value is int(200).
 */
if (!function_exists('http_response_code')) {
    function http_response_code($code = null) {
        if ($code !== null) {
            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 102: $text = 'Processing'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 207: $text = 'Multi-Status'; break;
                case 208: $text = 'Already Reported'; break;
                case 226: $text = 'IM Used'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 306: $text = 'Switch Proxy'; break;
                case 307: $text = 'Temporary Redirect'; break;
                case 308: $text = 'Permanent Redirect'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 416: $text = 'Requested Range Not Satisfiable'; break;
                case 417: $text = 'Expectation Failed'; break;
                case 420: $text = 'Enhance Your Calm'; break;
                case 422: $text = 'Unprocessable Entity'; break;
                case 423: $text = 'Locked'; break;
                case 424: $text = 'Failed Dependency'; break;
                case 424: $text = 'Method Failure'; break;
                case 425: $text = 'Unordered Collection'; break;
                case 426: $text = 'Upgrade Required'; break;
                case 428: $text = 'Precondition Required'; break;
                case 429: $text = 'Too Many Requests'; break;
                case 431: $text = 'Request Header Fields Too Large'; break;
                case 444: $text = 'No Response'; break;
                case 449: $text = 'Retry With'; break;
                case 450: $text = 'Blocked by Windows Parental Controls'; break;
                case 451: $text = 'Unavailable For Legal Reasons'; break;
                case 494: $text = 'Request Header Too Large'; break;
                case 495: $text = 'Cert Error'; break;
                case 496: $text = 'No Cert'; break;
                case 497: $text = 'HTTP to HTTPS '; break;
                case 499: $text = 'Client Closed Request'; break;
                case 494: $text = 'Request Header Too Large'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                case 506: $text = 'Variant Also Negotiates'; break;
                case 507: $text = 'Insufficient Storage'; break;
                case 508: $text = 'Loop Detected'; break;
                case 509: $text = 'Bandwidth Limit Exceeded'; break;
                case 510: $text = 'Not Extended'; break;
                case 511: $text = 'Network Authentication Required'; break;
                case 598: $text = 'Network read timeout error'; break;
                case 599: $text = 'Network connect timeout error'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            }
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $text);
            $GLOBALS['http_response_code'] = $code;
        } else {
            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
        }
        return $code;
    }
}

/**
 * Parse HTTP headers
 *
 * @param string $header - String containing HTTP headers 
 * @return array - Parsed headers
 */
function http_parse_headers($header) {
    $parsed_headers = array();
    $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $header));
    foreach ($fields as $field) {
        if (preg_match('/([^:]+): (.+)/m', $field, $match)) {
            //$match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
            $match[1] = strtoupper(trim($match[1]));
            if (isset($parsed_headers[$match[1]])) {
                if (!is_array($parsed_headers[$match[1]])) {
                    $parsed_headers[$match[1]] = array($parsed_headers[$match[1]]);
                }
                $parsed_headers[$match[1]][] = $match[2];
            } else {
                $parsed_headers[$match[1]] = trim($match[2]);
            }
        }
    }
    return $parsed_headers;
}