<?php
class LogException extends Exception {
    
    private $attachment;

    public function __construct($message = null, $code = 0, Exception $previous = null, $attachment = null) {
        $this->attachment = $attachment;
        parent::__construct($message, $code, $previous);
    }

    public function getAttachment() {
        return $this->attachment;
    }
}