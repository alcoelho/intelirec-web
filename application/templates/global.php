<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->import_jshead();?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <title><?php echo $this->get_title(); ?></title>
        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Intelirec">
        <meta name="msapplication-TileColor" content="#3372DF">
        <link rel="icon" href="<?php echo URI_IMAGES; ?>favicon.ico" />
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>libraries/coreui-icons.min.css">
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>libraries/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>libraries/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>libraries/simple-line-icons.css">
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>libraries/pace.min.css">
         <link rel="stylesheet" href="<?php echo URI_CSS; ?>plugins/easy-autocomplete.min.css">
        <link rel="stylesheet" href="<?php echo URI_CSS; ?>style.css">
        <?php $this->import_css();?>
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <div class="modal" tabindex="-1" role="dialog" id="msg_dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" id="msg_header">
                        <h5 class="modal-title" id="msg_title"></h5>
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span id="msg_msg"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <?php if (isset($_COOKIE['authorized'])) {?>
        
        <header class="app-header navbar">
            
            <?php $this->show_header();?>
        </header>
        <div class="app-body">
            <?php $this->show_menu();?>
            <main class="main">
                <div class="container-fluid">
                    <div class="animated fadeIn">
                        <?php $this->show_content();?>
                    </div>
                </div>
            </main>
            
            <?php } else { $this->show_content();}?>
        </div>
        <footer>
            <?php //$this->show_footer();?>
        </footer>
        <!-- Javascripts -->
        <script>
        // Constants
        var URI_API = '<?php echo URI_API; ?>';
        var URI_PANEL = '<?php echo URI_PANEL; ?>';
        var URI_IMAGES = '<?php echo URI_IMAGES; ?>';
        var URI_JS = '<?php echo URI_JS; ?>';
        var URI_LOG = '<?php echo URI_LOG; ?>';
        var URI_LOGIN = '<?php echo URI_LOGIN; ?>';
        var URI_DASHBOARD = '<?php echo URI_DASHBOARD; ?>';
        var URI_RECEITA_CADASTRO = '<?php echo URI_RECEITA_CADASTRO; ?>';
        var URI_RECEITA_HIST = '<?php echo URI_RECEITA_HIST; ?>';
        var URI_REGRA_CADASTRO = '<?php echo URI_REGRA_CADASTRO; ?>';
        var URI_REGRA_HIST = '<?php echo URI_REGRA_HIST; ?>';
        var COURIER_URI = '/couriers/' + localStorage.id;
        var I18N = 'pt-BR';
        var URI_ROOT = '<?php echo URI_ROOT; ?>';
        </script>
        <script src="<?php echo URI_JS; ?>libraries/jquery.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>libraries/popper.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>libraries/bootstrap.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>libraries/pace.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>libraries/perfect-scrollbar.min.js?ver=<?php echo APP_VERSION; ?>"></script>
         <script src="<?php echo URI_JS; ?>plugins/jquery.easy-autocomplete.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>libraries/coreui.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>helpers/apirequest.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>helpers/lang.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>web3.min.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>truffle-contract.js?ver=<?php echo APP_VERSION; ?>"></script>
        
        <script src="<?php echo URI_JS; ?>moment.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>uuid.js?ver=<?php echo APP_VERSION; ?>"></script>

        <script src="<?php echo URI_JS; ?>main.js?ver=<?php echo APP_VERSION; ?>"></script>
        <script src="<?php echo URI_JS; ?>blockchain.js?ver=<?php echo APP_VERSION; ?>"></script>
        
        <?php $this->import_js();?>
    </body>
</html>