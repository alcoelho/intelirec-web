<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
<span class="navbar-toggler-icon"></span>
</button>
<a class="navbar-brand" href="<?php echo URI_DASHBOARD;?>">
	<img class="navbar-brand-full" src="<?php echo URI_IMAGES; ?>intelirec.png" width="89" height="25" alt="CoreUI Logo">
	<img class="navbar-brand-minimized" src="<?php echo URI_IMAGES; ?>intelirec.png" width="30" height="30" alt="CoreUI Logo">
</a>
<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
<span class="navbar-toggler-icon"></span>
</button>
<h5 style="margin: 0 0 0 25px;"><?php echo $this->get_page_title(); ?></h5>
<ul class="nav navbar-nav ml-auto">
	<li class="nav-item dropdown">
		<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			<span id="email-login"></span><img class="img-avatar" src="<?php echo URI_IMAGES; ?>icon_user.png" alt="">
		</a>
		<div class="dropdown-menu dropdown-menu-right">
			<div class="dropdown-header text-center">
				<strong>Ações</strong>
			</div>
			<a class="dropdown-item" href="javascript:logout();">
			<i class="fa fa-lock"></i> Sair do sistema</a>
		</div>
	</li>
</ul>