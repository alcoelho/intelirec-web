<div class="sidebar">
	<nav class="sidebar-nav">
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo URI_DASHBOARD;?>">
					<i class="nav-icon icon-speedometer"></i> Dashboard
				</a>
			</li>
			<li class="nav-title">Ações</li>
			<li class="nav-item nav-dropdown">
				<a class="nav-link nav-dropdown-toggle" href="#">
				<i class="nav-icon icon-puzzle"></i> Receitas</a>
				<ul class="nav-dropdown-items">
					<li class="nav-item" permissions="MED">
						<a class="nav-link" href="<?php echo URI_RECEITA_CADASTRO;?>">
						<i class="nav-icon icon-puzzle"></i> Cadastrar</a>
					</li>
					<li class="nav-item" permissions="MED,FARMA,ANVISA">
						<a class="nav-link" href="<?php echo URI_RECEITA_HIST;?>">
						<i class="nav-icon icon-puzzle"></i> Histórico</a>
					</li>
				</ul>
			</li>
			<li class="nav-item nav-dropdown" permissions="ANVISA">
				<a class="nav-link nav-dropdown-toggle" href="#">
				<i class="nav-icon icon-puzzle"></i> Regras</a>
				<ul class="nav-dropdown-items">
					<li class="nav-item" permissions="ANVISA">
						<a class="nav-link" href="<?php echo URI_REGRA_CADASTRO;?>">
						<i class="nav-icon icon-puzzle"></i> Cadastrar</a>
					</li>
					<li class="nav-item" permissions="ANVISA">
						<a class="nav-link" href="<?php echo URI_REGRA_HIST;?>">
						<i class="nav-icon icon-puzzle"></i> Histórico</a>
					</li>
				</ul>
			</li>
		</ul>
	</nav>
	<button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>