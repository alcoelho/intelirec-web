<!-- <section id="headline">
    <div class="container">
        <div id="logo_wrapper">
            <img id="company_logo" src="" alt="" />
        </div>
        <h1 id="company_name"></h1>
    </div>
</section>
 -->
<section id="headline">
    <div class="container">
        <div class="col-md-10">
        	<div id="headline-company-logo">
        		<img id="company_logo"/>
        	</div>
        	<div id="headline-title" class="shadow-box round-box">
           		<h1 id="company_name">Company Name</h1>
           </div>
        </div>
        <div id="headline-logo" class="col-md-2 text-right">
            <img src="/img/logo.png" width="87" height="72" alt="Huru" />
        </div>
    </div>
</section>
<audio id="notification"  preload="none"></audio>