<?php
ob_start();

require_once(dirname(__FILE__) . '/constants.php');
require_once(PATH_HELPERS . 'functions.php');

global $config;

$config['api_username'] = 'admin@admin.com';
$config['api_password'] = 'test1234';

/**********************************************************************
 * Logs
 *********************************************************************/

// Enable or disable error logging
$config['log_enabled'] = true;
// Enable or disable the DEBUG mode
$config['log_debug'] = true;
// E-mail address to send the error logs
$config['log_email'] = 'andre.coelho@nkey.com.br';
// From
$config['log_from'] = array('formulario@nkey.com.br' => 'NicTrack Reporter');


/**********************************************************************
 * Mail
 *********************************************************************/

// Host of the SMTP server
$config['smtp_host'] = 'smtp.gmail.com';
// SMTP port
$config['smtp_port'] = 465;
// SMTP account username                        
$config['smtp_username'] = 'formulario@nkey.com.br';
// SMTP account password
$config['smtp_password'] = 'nkey1078';

/**********************************************************************
 * Google Maps Key
 *********************************************************************/

$config['google_maps_api_key'] = 'AIzaSyC9laIvz69kPc1QZlUQmbIl4hglZOA7O7w';
//$config['google_maps_api_key'] = 'AIzaSyBS77jBDWkHPUYhog9A-XJzSosiHpOLWMw';


/**********************************************************************
 * Error/Exception Handling
 *********************************************************************/

// Define the function to handle exceptions
set_exception_handler('handle_exception');
//Define the function to handle errors
set_error_handler('handle_error');
//Sets which PHP errors are reported
error_reporting(E_ALL);
//error_reporting(0);
//Turn error displaying on or off
ini_set('display_errors', '1');


/**********************************************************************
 * Date and time
 *********************************************************************/

date_default_timezone_set('UTC');