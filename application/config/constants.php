<?php

// Environment
define('ENVIRONMENT_DEV', 'DEV');
define('ENVIRONMENT_PROD', 'PROD');

$env = getenv("ENVIRONMENT_TYPE");

switch ($env) {
	case 'DEV':
		define('ENVIRONMENT_TYPE', ENVIRONMENT_DEV);
		require_once('constants.template.dev.php');		
		break;
	case 'DEMO':
		define('ENVIRONMENT_TYPE', ENVIRONMENT_DEV);
		require_once('constants.template.demo.php');		
		break;
	case 'PROD':
		define('ENVIRONMENT_TYPE', ENVIRONMENT_PROD);
		require_once('constants.template.prod.php');		
		break;		
	default:
		define('ENVIRONMENT_TYPE', ENVIRONMENT_DEV);
		require_once('constants.template.loc.php');	
		break;
}