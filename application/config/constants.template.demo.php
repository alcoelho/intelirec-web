<?php 
// Paths:

define('APP_VERSION', '1.1.0');

define('PATH_APPLICATION', realpath(dirname(__FILE__) . '/../'));
define('PATH_PUBLIC', realpath(dirname(__FILE__) . '/../../public') . '/');

// Application paths
define('PATH_CONFIG', PATH_APPLICATION . '/config/');
define('PATH_HELPERS', PATH_APPLICATION . '/helpers/');
define('PATH_SCRIPTS', PATH_APPLICATION . '/scripts/');
define('PATH_LIBRARIES', PATH_APPLICATION . '/libraries/');
define('PATH_LOGS', PATH_APPLICATION . '/logs/');
define('PATH_TEMPLATES', PATH_APPLICATION . '/templates/');
define('PATH_TEMPORARY', PATH_APPLICATION . '/temporary/');
define('PATH_CSS', PATH_PUBLIC . '/_css/');

// Logs
define('LOGS_ERROR', PATH_LOGS . 'error.log');
define('LOGS_MAIL', PATH_LOGS . 'mail.log');

// Scripts
//define('SCRIPT_SENDMAIL', PATH_SCRIPTS . 'sendmail.php');

// Template
define('DEFAULT_TITLE', 'Huru Systems');
define('DEFAULT_GLOBAL_FILE', 'global.php');
define('DEFAULT_HEADER_FILE', 'header.php');
define('DEFAULT_MENU_FILE', 'menu.php');
define('DEFAULT_BODY_FILE', 'body.php');
define('DEFAULT_FOOTER_FILE', 'footer.php');

// Global URIs:
//define('URI_ROOT_COMPONENTS', '/Projects/nic-web/public/');
define('URI_ROOT_COMPONENTS', '/');
define('URI_PANEL_COMPONENTS', URI_ROOT_COMPONENTS);
define('PROTOCOL', 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://');
define('PORT', '');
define('URI_ROOT', URI_ROOT_COMPONENTS);
define('URI_PANEL', URI_ROOT_COMPONENTS);
define('URI_API', 'http://demo.huru.nkey.com.br:8443');
define('URI_CSS', URI_ROOT_COMPONENTS . '_css/');
define('URI_JS', URI_ROOT_COMPONENTS . '_js/');
define('URI_IMAGES', URI_ROOT_COMPONENTS . '_img/');
define('URI_LOG', URI_ROOT_COMPONENTS . '_log/');
// define('URI_ERROR_401', URI_PANEL . '401/');
// define('URI_ERROR_403', URI_PANEL . '403/');
// define('URI_ERROR_404', URI_PANEL . '404/');
// define('URI_ERROR_500', URI_PANEL . '500/');

// URIs:
define('URI_LOGIN', URI_ROOT . 'login/');
define('URI_ORDERS', URI_ROOT . 'login/');
define('URI_USER', URI_ROOT . 'user/');
define('URI_DASHBOARD', URI_ROOT . 'dashboard/');
define('URI_INVENTORY', URI_ROOT . 'inventory/');
define('URI_DEPARTMENT', URI_ROOT . 'department/');
define('URI_REPORTS', URI_ROOT . 'reports/');
define('URI_MATERIAL', URI_ROOT . 'material/');
define('URI_PROVIDER', URI_ROOT . 'provider/');
define('URI_DEVICE', URI_ROOT . 'device/');
define('URI_TRANSACTION', URI_ROOT . 'transaction/');
