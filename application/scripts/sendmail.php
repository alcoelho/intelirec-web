<?php
// Print the output message
function output($message) {
    global $argv;
    $output = array(
        'date' => gmdate('Y-m-d H:i:s'),
        'message' => $message,
        'parameters' => $argv
    );
    echo json_encode($output) . "\n";
    // echo gmdate('Y-m-d') . "\t" . $message . "\t" . print_r($argv, true);
}
// Wrap in a try/catch block to avoid recursion (error/exception -> send mail -> error/exception -> ...)
try {
    require_once(dirname(__FILE__) . '/../config/global.php');
    // Get the parameters passed in command line
    $parameters = array();
    $parameters['from'] = isset($argv[1]) ? $argv[1] : null;
    $parameters['to'] = isset($argv[2]) ? $argv[2] : null;
    $parameters['subject'] = isset($argv[3]) ? $argv[3] : null;
    $parameters['message'] = isset($argv[4]) ? $argv[4] : null;
    $parameters['attachment'] = isset($argv[5]) ? $argv[5] : null;
    $parameters['from_name'] = isset($argv[6]) ? $argv[6] : null;
    $parameters['to_name'] = isset($argv[7]) ? $argv[7] : null;
    // Check for missing parameters
    $index = 1;
    $optional = array('attachment', 'from_name', 'to_name');
    foreach ($parameters as $key=>$value) {
        if (!$value && !in_array($key, $optional)) {
            output('"' . $value . '" (' . $index . ') must be specified');
            exit(1);
        }
        $index++;
    }
    // Validate
    if (!file_exists($parameters['message'])) {
        output('"message" (4) file not found.');
        exit(1);
    }
    if ($parameters['attachment'] && !file_exists($parameters['attachment'])) {
        output('"attachment" (5) file not found');
        exit(1);
    }
    // Normalize
    $parameters['from'] = $parameters['from_name'] ? array($parameters['from'] => $parameters['from_name']) : $parameters['from'];
    $parameters['to'] = $parameters['to_name'] ? array($parameters['to'] => $parameters['to_name']) : $parameters['to'];
    $parameters['message'] = file_get_contents($parameters['message']);
    // Send the email
    if (send_mail($parameters['from'], $parameters['to'], $parameters['subject'], $parameters['message'] , $parameters['attachment'])) {
        exit(0);
    } else {
        output('Failed to send the email');
        exit(1);
    }
} catch (Exception $e) {
    output($e->getMessage());
    exit(1);
}